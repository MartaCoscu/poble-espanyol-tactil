using System;

namespace Tuio
{
    /// <summary>
    /// Stores advanced properties about touch points.
    /// NOTE: Currently limited to just a few properties but can easily be expanded.
    /// </summary>
    public struct TouchProperties
    {
        public double Acceleration;
        public double VelocityX;
		public double VelocityY;
		public float Height;
        public float Width;
    }
}