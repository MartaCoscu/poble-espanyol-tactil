﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class LoadLanguage : MonoBehaviour {

	public string url;
	public string lang; 
	public SelectLanguage languages; 
	void OnEnable () {
		if (url != "" && lang != "") {
			Load (url, lang);
		}
	}


	public void Load(string myUrl, string myLang){	
		url = myUrl;
		lang = myLang;

		if (gameObject.activeInHierarchy) {
			StartCoroutine (PPT.LoadInto (("file://" + PPT.GetPath (lang + "/" + url)), gameObject, null, false));
		}
	}

	public void Click(){
		languages.SetIdioma (int.Parse (lang)); 
	}
}
