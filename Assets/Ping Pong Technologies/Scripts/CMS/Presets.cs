﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml.Serialization;
using System.Collections.Generic;

[XmlRoot("presets")]
public class Presets
{

    [XmlArray]
    [XmlArrayItem("preset")]
    public List<Preset> presets = new List<Preset>();

    private Preset presetActual;

    public delegate void OnLoadedContent();
    public static OnLoadedContent onLoadedContent;

    public static Preset GetActualPreset()
    {
        if (GetInstance().presetActual == null)
        {
            //carreguem el preset que toca....
            foreach (Preset p in GetInstance().presets)
            {
                if (p.selected.CompareTo("1") == 0)
                {
                    GetInstance().presetActual = p;
                    break;
                }
            }
        }
        return GetInstance().presetActual;
    }

    private static Presets instance;
    public static Presets GetInstance()
    {
        if (instance == null)
        {
            instance = PPT.LoadSerializer<Presets>(Application.streamingAssetsPath + "/presets.xml");
        }

        if (instance == null)
        {
            instance = new Presets();
        }
        return instance;
    }


    public static void LoadContent()
    {
        GetActualPreset();
        if (onLoadedContent != null)
        {
            onLoadedContent();
        }
    }
}
