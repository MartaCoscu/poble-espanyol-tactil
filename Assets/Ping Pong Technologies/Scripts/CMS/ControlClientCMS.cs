﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Xml.Serialization;
using System;

public class ControlClientCMS : MonoBehaviour
{

    public delegate void OnFileLckCreated();
    public static OnFileLckCreated onFileLckCreated;

    public delegate void OnFileLckDeleted();
    public static OnFileLckDeleted onFileLckDeleted;

    private float tmpTimeRefreshLckFile;
    private bool testIfExists = true;

    public bool demo, check;


    public GameObject emptyGameObjectPrefab;
    // Use this for initialization
    void Start()
    {
        System.GC.Collect();
        tmpTimeRefreshLckFile = Time.time;
        PPT.Serializer<ControlCMS>(Application.streamingAssetsPath + "/ControlClientCMS.xml", ControlCMS.GetInstance());

        if (!GameObject.Find("NetworkPPTClient"))
        {
            //inicialitem client de xarxa...
            GameObject c = Instantiate<GameObject>(emptyGameObjectPrefab);
            c.name = "NetworkPPTClient";
            //NetworkPPTClient d = c.AddComponent<NetworkPPTClient>();

        }

    }

    // Update is called once per frame
    void Update()
    {
        if (tmpTimeRefreshLckFile + ControlCMS.GetInstance().timeRefreshLckFile < Time.time)
        {
            tmpTimeRefreshLckFile = Time.time;
            //TESTEM SI EXISTEIX FILE LCK

            if (testIfExists)
            {
                if (File.Exists(Application.streamingAssetsPath + "/lock.lck"))
                {
                    if (onFileLckCreated != null)
                    {
                        onFileLckCreated();
                        testIfExists = false;
                    }
                }
            }
            else {
                if (!File.Exists(Application.streamingAssetsPath + "/lock.lck"))
                {
                    if (onFileLckDeleted != null)
                    {
                        Presets.LoadContent();
                        onFileLckDeleted();
                        testIfExists = true;
                    }
                }
            }
        }

        if (demo && check)
        {
            Presets.LoadContent();
            onFileLckDeleted();
            testIfExists = true;
            check = false;
        }



    }
}
