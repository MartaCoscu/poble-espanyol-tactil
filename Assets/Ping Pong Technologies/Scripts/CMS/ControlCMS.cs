﻿using System;
using System.Xml.Serialization;
using UnityEngine;

[XmlRoot]
public class ControlCMS
{
    [XmlElement]
    public float timeRefreshLckFile;

    private static ControlCMS instance;

    public static ControlCMS GetInstance()
    {
        if (instance == null)
        {
            try
            {
                instance = PPT.LoadSerializer<ControlCMS>(Application.streamingAssetsPath + "/ControlCMS.xml");
            }
            catch (Exception e)
            {

            }
            if (instance == null)
            {
                instance = new ControlCMS();
            }
        }
        return instance;
    }
}