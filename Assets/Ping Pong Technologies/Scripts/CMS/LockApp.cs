﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class LockApp : MonoBehaviour
{

    public PPTAnimation activateBackgroundProtection;

    // Use this for initialization
    void Start()
    {
        ControlClientCMS.onFileLckCreated += OnLckCreated;
        ControlClientCMS.onFileLckDeleted += OnLckDeleted;
    }

    private void OnLckDeleted()
    {
        Debug.Log("OnLckDeleted");
        activateBackgroundProtection.inverse = true;
        activateBackgroundProtection.onCompleteAnimationAction = () =>
        {
            activateBackgroundProtection.gameObject.SetActive(false);
            //Scene scene = SceneManager.GetActiveScene();
            //SceneManager.LoadScene(scene.name);
        };
        activateBackgroundProtection.LaunchAnimation();
    }

    private void OnLckCreated()
    {
        Debug.Log("OnLckCreated");
        activateBackgroundProtection.gameObject.SetActive(true);
        activateBackgroundProtection.inverse = false;
        activateBackgroundProtection.onCompleteAnimationAction = null;
        activateBackgroundProtection.LaunchAnimation();
    }

    public void OnDestroy()
    {
        ControlClientCMS.onFileLckCreated -= OnLckCreated;
        ControlClientCMS.onFileLckDeleted -= OnLckDeleted;
    }
}
