﻿
using System.Xml.Serialization;

[XmlRoot]
public class Preset
{

    [XmlElement]
    public string id = "";

    [XmlElement]
    public string selected = "";
}