﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class ScreenManager : MonoBehaviour
{
    public AbstractScreenApplication[] screens;
    public AbstractScreenApplication firstScreen;
    public AbstractScreenApplication mainScreen;
    public AbstractScreenApplication screenSaver;

    public bool closeScreenSaverOnTouch = false;
    public float screenSaverTime;

    public static AbstractScreenApplication screenActual;
    private static AbstractScreenApplication screenAnterior;
    public static bool lockStatus;

    private static List<AbstractScreenApplication> screensInstance = new List<AbstractScreenApplication>();


    private float timeToLaunchScreenSaver;
    private bool screenSaverActivated;


    public bool forceChangeScreen = false;
    public AbstractScreenApplication screenToLoad;
    public bool bloq;

    public bool screenApplicationLoadAtSameTime = false;

    private static ScreenManager instance;

    public static void LoadScreenApplication(string v)
    {
        //en cas de no tenir la pantalla bloquejada...
        if (!instance.bloq)
        {
            //bloquejem pantalla
            instance.bloq = true;

            //la actual passa a ser l'anterior...
            ScreenManager.screenAnterior = ScreenManager.screenActual;

            AbstractScreenApplication next = null;

            foreach (AbstractScreenApplication s in screensInstance)
            {
                //Debug.Log("name: " + s.name + ", v:" + v);
                if (s.name.CompareTo(v) == 0)
                {
                    next = s;
                    break;
                }
            }

            if (next != null)
            {
                //la actual passa a ser la next...
                ScreenManager.screenActual = next;

                ScreenManager.screenActual.onLoadInternalApplication = () =>
                {
                    instance.bloq = false;
                };

                if (instance.screenApplicationLoadAtSameTime)
                {
                    ScreenManager.screenActual.LoadScreenApplication();

                    ScreenManager.screenAnterior.onUnLoadInternalApplication = null;
                }
                else {
                    ScreenManager.screenAnterior.onUnLoadInternalApplication = () =>
                    {
                        ScreenManager.screenActual.LoadScreenApplication();

                    };
                }
                ScreenManager.screenAnterior.UnLoadScreenApplication();
            }
        }
    }

    internal static bool IsBlock()
    {
        return instance.bloq;
    }

    public void LoadScreenApplicationEvent(string v)
    {
        LoadScreenApplication(v);
    }

    public void Update()
    {
        /*bloq = ScreenManager.lockStatus;

        if (screenActual != screenToLoad && forceChangeScreen)
        {
            forceChangeScreen = false;
            //hi ha un canvi manual de pantalla...
            LoadScreenApplication(screenToLoad);
        }

        ScreenSaverTime();*/
    }

    private void ScreenSaverTime()
    {
        //Screen Saver
        if (Input.touchCount > 0 || InputProxy.touchCount > 0 || Input.GetMouseButton(0) || Input.GetMouseButton(1) || Input.GetMouseButton(2))
        {
            //estem funcionant amb pantalla normal...
            timeToLaunchScreenSaver = Time.time;

            if (screenSaverActivated)
            {
                screenSaverActivated = false;
                if (closeScreenSaverOnTouch)
                {
                    if (screenActual != mainScreen)
                        //s'ha de trencar la pantalla de screen Saver i carregar main meunu
                        LoadScreenApplication(mainScreen);
                }
            }
        }

        if (timeToLaunchScreenSaver + screenSaverTime < Time.time && !screenSaverActivated)
        {
            {
                screenSaverActivated = true;
                if (screenActual != screenSaver)
                {
                    //hi ha un canvi manual de pantalla...
                    LoadScreenApplication(screenSaver);
                }
            }
        }
    }

    public void Start()
    {
        instance = this;

        screensInstance.Clear();
        //anem a carregar pantalla
        foreach (AbstractScreenApplication s in screens)
        {
            s.gameObject.SetActive(false);
            screensInstance.Add(s);
            s.OnAwakeScreenApplication();
        }

        firstScreen.LoadScreenApplication();

        screenActual = firstScreen;
        screenToLoad = screenActual;

        //save screen
        timeToLaunchScreenSaver = Time.time;
    }

    public static void LoadScreenApplication(AbstractScreenApplication next)
    {
        LoadScreenApplication(next.gameObject.name);
    }
}
