﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


[RequireComponent(typeof(CanvasGroup))]
public abstract class AbstractScreenApplication : MonoBehaviour
{
    public bool goToUnLoad;
    public bool goToLoad;

    public delegate void OnLoadApplication();
    public OnLoadApplication onLoadApplication;

    public delegate void OnUnLoadApplication();
    public OnUnLoadApplication onUnLoadApplication;


    public delegate void OnLoadInternalApplication();
    public OnLoadInternalApplication onLoadInternalApplication;

    public delegate void OnUnLoadInternalApplication();
    public OnUnLoadInternalApplication onUnLoadInternalApplication;

    protected void OnLoadDone()
    {
        Debug.Log("Load->" + gameObject.name);
        if (onLoadApplication != null)
        {
            onLoadApplication();
        }
        if (onLoadInternalApplication != null)
        {
            onLoadInternalApplication();
        }
    }

    protected void OnUnloadDone()
    {
        Debug.Log("UnLoad->" + gameObject.name);
        if (onUnLoadApplication != null)
        {
            onUnLoadApplication();
        }
        if (onUnLoadInternalApplication != null)
        {
            onUnLoadInternalApplication();
        }
    }



    public void Update()
    {
        if (goToLoad)
        {
            goToLoad = false;
            LoadScreenApplication();
        }

        if (goToUnLoad)
        {
            goToUnLoad = false;
            UnLoadScreenApplication();
        }

        if (ScreenManager.IsBlock())
        {
            GetComponent<CanvasGroup>().interactable = false;
        }
        else {
            GetComponent<CanvasGroup>().interactable = true;
        }
    }

    public abstract void LoadScreenApplication();
    public abstract void OnAwakeScreenApplication();
    public abstract void UnLoadScreenApplication();
}
