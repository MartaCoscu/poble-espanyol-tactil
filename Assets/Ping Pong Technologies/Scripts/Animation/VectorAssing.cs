﻿using System;
using UnityEngine;

[Serializable]
public class VectorAssign
{
    public Vector3 vector;
    public bool useObjectVector;
    public bool assignCurrent;
}

[Serializable]
public class FloatAssign
{
    public float floatValue;
    public bool useObjectVector;
    public bool assignCurrent;
}