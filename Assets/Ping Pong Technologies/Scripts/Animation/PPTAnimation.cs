﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
[ExecuteInEditMode]
public class PPTAnimation : MonoBehaviour
{
    public GameObject gameObjectForAnimation;

    public float time = 1;
    public float delay = 0;
    public LeanTweenType loopType = LeanTweenType.once;
    public LeanTweenType easeType = LeanTweenType.easeInOutSine;
    public AnimationCurve animationCurve;

    public int loopCount = 0;
    public bool launchOnEnable = false;


    [Range(0, 1)]
    public float value = 0;

    public bool attachToValue = false;
    public bool inverse;
    //public GameObject[] notifyGameObjects;
    //public string[] messages;

    public Action onCompleteAnimationAction = null;

    [SerializeField]
    public PPTModules modules = new PPTModules();
    private LTDescr description;

    //private bool launchAnimation;

    public void PauseAnimation()
    {
        description.pause();
    }
    public void StopAnimation()
    {
        LeanTween.cancel(description.id);
    }

    public void LaunchAnimation()
    {
        if (Application.isPlaying)
        {


            value = inverse ? 1 : 0;

            GameObject g = gameObjectForAnimation;

            modules.OnEnableAnimation(this);
            //LeanTween.cancel(g);

            if (easeType == LeanTweenType.animationCurve)
            {
                description = LeanTween.value(g, (float f) =>
                {
                    value = f;
                    //OnUpdateAnimation(this);
                    modules.Update(this, f);
                }, inverse ? 1 : 0, inverse ? 0 : 1, time)
                .setDelay(delay)
                .setLoopCount(loopCount)
                .setLoopType(loopType)
                .setOnComplete(() =>
                {
                    modules.OnComplete(this);

                    if (onCompleteAnimationAction != null)
                    {
                        onCompleteAnimationAction();
                    }

                })
                .setEase(animationCurve);
            }
            else {
                description = LeanTween.value(g, (float f) =>
               {
                   value = f;
                   //OnUpdateAnimation(this);
                   modules.Update(this, f);
               }, inverse ? 1 : 0, inverse ? 0 : 1, time)
                .setDelay(delay)
                .setLoopCount(loopCount)
                .setLoopType(loopType)
                .setOnComplete(() =>
                {
                    modules.OnComplete(this);

                    if (onCompleteAnimationAction != null)
                    {
                        onCompleteAnimationAction();
                    }

                })
                .setEase(easeType);
            }
        }
    }

    public void OnEnable()
    {
        if (launchOnEnable)
        {
            //launchAnimation = true;
            LaunchAnimation();
        }
    }

    public void Update()
    {
        if (!Application.isPlaying)
        {
            if (gameObjectForAnimation == null)
                gameObjectForAnimation = gameObject;

            modules.Update(this, value);
        }
    }
}





