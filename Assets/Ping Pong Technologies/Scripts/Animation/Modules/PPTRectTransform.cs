﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class PPTRectTransform : PPTModule
{
    [SerializeField]
    public VectorAssign firstScale;
    [SerializeField]
    public VectorAssign lastScale;

    public override void OnCompleteAnimation()
    {
        //throw new NotImplementedException();
    }

    public override void Update(PPTAnimation g1, float f)
    {
        {
            bool attach = g1.attachToValue;
            GameObject g = g1.gameObjectForAnimation;

            if (firstScale.assignCurrent)
            {
                firstScale.assignCurrent = false;

                if (g.GetComponent<RectTransform>())
                {
                    firstScale.vector = g.GetComponent<RectTransform>().sizeDelta;
                }
            }

            if (lastScale.assignCurrent)
            {
                lastScale.assignCurrent = false;
                if (g.GetComponent<RectTransform>())
                {
                    lastScale.vector = g.GetComponent<RectTransform>().sizeDelta;
                }
            }

            if (attach || Application.isPlaying)
            {
                if (g.GetComponent<RectTransform>())
                {
                    g.GetComponent<RectTransform>().sizeDelta = (firstScale.vector + ((lastScale.vector - firstScale.vector) * f));
                }
            }

            
        }
    }

    internal override void OnEnableAnimation(PPTAnimation a)
    {
        if (a.attachToValue || Application.isPlaying)
            if (a.inverse)
            {
                if (!lastScale.useObjectVector)
                    a.gameObjectForAnimation.GetComponent<RectTransform>().sizeDelta = (lastScale.vector);
                else
                    lastScale.vector = a.GetComponent<RectTransform>().sizeDelta;
            }
            else {
                if (!firstScale.useObjectVector)
                    a.gameObjectForAnimation.GetComponent<RectTransform>().sizeDelta = (firstScale.vector);
                else
                    firstScale.vector = a.gameObjectForAnimation.GetComponent<RectTransform>().sizeDelta;
            }


        if ( (a.attachToValue || Application.isPlaying))
            if (a.inverse)
            {
                if (!lastScale.useObjectVector)
                    a.gameObjectForAnimation.GetComponent<RectTransform>().sizeDelta = (lastScale.vector);
                else
                    lastScale.vector = a.gameObjectForAnimation.GetComponent<RectTransform>().sizeDelta;
            }
            else {
                if (!firstScale.useObjectVector)
                    a.gameObjectForAnimation.GetComponent<RectTransform>().sizeDelta = (firstScale.vector);
                else
                    firstScale.vector = a.gameObjectForAnimation.GetComponent<RectTransform>().sizeDelta;
            }
    }
}
