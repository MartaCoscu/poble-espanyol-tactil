﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class PPTOnCompleteModule : PPTModule
{
    public GameObject[] notifyGameObjects;
    public String[] messages;

    public override void OnCompleteAnimation()
    {
        int pos = 0;
        foreach (GameObject g1 in notifyGameObjects)
        {
            //Debug.Log("game+" + g1.name);
            g1.SendMessage(messages[pos], SendMessageOptions.DontRequireReceiver);
            pos++;
        }
    }

    public override void Update(PPTAnimation g1, float f)
    {
    }

    internal override void OnEnableAnimation(PPTAnimation a)
    {
    }
}
