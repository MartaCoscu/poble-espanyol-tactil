﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]

public class PPTModules
{

    [SerializeField]
    public PPTPositionModule position = new PPTPositionModule();
    [SerializeField]
    public PPTRectTransform rectTransform = new PPTRectTransform();
    [SerializeField]
    public PPTGlobalPositionModule globalPosition = new PPTGlobalPositionModule();
    [SerializeField]
    public PPTOnCompleteModule onComplete = new PPTOnCompleteModule();
    [SerializeField]
    public PPTScaleModule scale = new PPTScaleModule();
    [SerializeField]
    public PPTAlphaModule alpha = new PPTAlphaModule();
    [SerializeField]
    public PPTRotationModule rotation = new PPTRotationModule();
    [SerializeField]
    public PPTPathStatus paths = new PPTPathStatus();

    private List<PPTModule> activeModules = new List<PPTModule>();
    private PPTAnimation pPTAnimation;


    public PPTModules()
    {
        activeModules.Clear();
        activeModules.Add(position);
        activeModules.Add(rectTransform);
        activeModules.Add(globalPosition);
        activeModules.Add(onComplete);
        activeModules.Add(scale);
        activeModules.Add(alpha);
        activeModules.Add(rotation);
        activeModules.Add(paths);
    }

    public void Update(PPTAnimation p1, float f)
    {
        foreach (PPTModule p in activeModules)
        {
            if (p.use)
                p.Update(p1, f);
        }
    }

    public void OnEnableAnimation(PPTAnimation p1)
    {
        foreach (PPTModule p in activeModules)
        {
            if (p.use)
                p.OnEnableAnimation(p1);
        }
    }

    internal void OnComplete(PPTAnimation p1)
    {
        foreach (PPTModule p in activeModules)
        {
            if (p.use)
                p.OnCompleteAnimation();
        }
    }
}