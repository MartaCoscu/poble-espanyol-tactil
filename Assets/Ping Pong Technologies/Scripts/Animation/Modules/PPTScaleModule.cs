﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class PPTScaleModule : PPTModule
{
    [SerializeField]
    public VectorAssign firstScale;
    [SerializeField]
    public VectorAssign lastScale;

    public override void OnCompleteAnimation()
    {
        //throw new NotImplementedException();
    }

    public override void Update(PPTAnimation g1, float f)
    {
        {
            bool attach = g1.attachToValue;
            GameObject g = g1.gameObjectForAnimation;

            if (firstScale.assignCurrent)
            {
                firstScale.assignCurrent = false;

                if (g.GetComponent<RectTransform>())
                {
                    firstScale.vector = g.GetComponent<RectTransform>().localScale;
                }
                else
                {
                    firstScale.vector = g.transform.localScale;
                }
            }

            if (lastScale.assignCurrent)
            {
                lastScale.assignCurrent = false;
                if (g.GetComponent<RectTransform>())
                {
                    lastScale.vector = g.GetComponent<RectTransform>().localScale;
                }
                else
                {
                    lastScale.vector = g.transform.localScale;
                }
            }

            if (attach || Application.isPlaying)
            {
                if (g.GetComponent<RectTransform>())
                {
                    g.GetComponent<RectTransform>().localScale = (firstScale.vector + ((lastScale.vector - firstScale.vector) * f));
                }
                else
                {
                    g.transform.localScale = (firstScale.vector + ((lastScale.vector - firstScale.vector) * f));
                }
            }

            
        }
    }

    internal override void OnEnableAnimation(PPTAnimation a)
    {
        if (a.attachToValue || Application.isPlaying)
            if (a.inverse)
            {
                if (!lastScale.useObjectVector)
                    a.gameObjectForAnimation.transform.localScale = (lastScale.vector);
                else
                    lastScale.vector = a.gameObjectForAnimation.transform.localScale;
            }
            else {
                if (!firstScale.useObjectVector)
                    a.gameObjectForAnimation.transform.localScale = (firstScale.vector);
                else
                    firstScale.vector = a.gameObjectForAnimation.transform.localScale;
            }


        if ( (a.attachToValue || Application.isPlaying))
            if (a.inverse)
            {
                if (!lastScale.useObjectVector)
                    a.gameObjectForAnimation.transform.localScale = (lastScale.vector);
                else
                    lastScale.vector = a.gameObjectForAnimation.transform.localScale;
            }
            else {
                if (!firstScale.useObjectVector)
                    a.gameObjectForAnimation.transform.localScale = (firstScale.vector);
                else
                    firstScale.vector = a.gameObjectForAnimation.transform.localScale;
            }
    }
}
