﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public abstract class PPTModule  {

    [SerializeField]
    public bool use;

    public abstract  void Update(PPTAnimation g, float f);
    internal abstract void OnEnableAnimation(PPTAnimation p1);
    public abstract void OnCompleteAnimation();
}
