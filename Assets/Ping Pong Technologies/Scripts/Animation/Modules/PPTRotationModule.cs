﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class PPTRotationModule : PPTModule
{
    [SerializeField]
    public VectorAssign firstPosition;
    [SerializeField]
    public VectorAssign lastPosition;

    public override void OnCompleteAnimation()
    {
        //throw new NotImplementedException();
    }

    public override void Update(PPTAnimation g1, float f)
    {
        bool attach = g1.attachToValue;
        GameObject g = g1.gameObjectForAnimation;


        if (firstPosition.assignCurrent)
        {
            firstPosition.assignCurrent = false;
            firstPosition.vector = (g.transform.rotation).eulerAngles;
        }

        if (lastPosition.assignCurrent)
        {
            lastPosition.assignCurrent = false;
            lastPosition.vector = (g.transform.rotation).eulerAngles;
        }


        if (attach || Application.isPlaying)
            g.transform.localRotation = Quaternion.Euler(firstPosition.vector + ((lastPosition.vector - firstPosition.vector) * f));



    }

    internal override void OnEnableAnimation(PPTAnimation a)
    {
        if (a.attachToValue || Application.isPlaying)
            if (a.inverse)
            {
                if (!lastPosition.useObjectVector)
                    a.gameObjectForAnimation.transform.rotation = Quaternion.Euler(lastPosition.vector);

                else
                    lastPosition.vector = a.gameObjectForAnimation.transform.rotation.eulerAngles;// = Quaternion.Euler(a.rotation.lastDegrees.vector);
            }
            else {
                if (!firstPosition.useObjectVector)
                    a.gameObjectForAnimation.transform.rotation = Quaternion.Euler(firstPosition.vector);

                else
                    firstPosition.vector = a.gameObjectForAnimation.transform.rotation.eulerAngles;// = Quaternion.Euler(lastPosition.vector);
            }


        /*if (a.attachToValue || Application.isPlaying)
            if (a.inverse)
            {
                if (!lastPosition.useObjectVector)
                    a.gameObjectForAnimation.transform.localPosition = (lastPosition.vector);
                else
                    lastPosition.vector = a.gameObjectForAnimation.transform.localPosition;
            }
            else {
                if (!firstPosition.useObjectVector)
                    a.gameObjectForAnimation.transform.localPosition = (firstPosition.vector);
                else
                    firstPosition.vector = a.gameObjectForAnimation.transform.localPosition;
            }*/
    }
}
