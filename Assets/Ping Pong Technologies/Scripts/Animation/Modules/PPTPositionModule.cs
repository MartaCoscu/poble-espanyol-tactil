﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class PPTPositionModule : PPTModule
{
    [SerializeField]
    public VectorAssign firstPosition;
    [SerializeField]
    public VectorAssign lastPosition;

    public override void OnCompleteAnimation()
    {
        //throw new NotImplementedException();
    }

    public override void Update(PPTAnimation g1, float f)
    {
        {
            bool attach = g1.attachToValue;
            GameObject g = g1.gameObjectForAnimation;

            if (firstPosition.assignCurrent)
            {
                firstPosition.assignCurrent = false;

                if (g.GetComponent<RectTransform>())
                {
                    firstPosition.vector = g.GetComponent<RectTransform>().localPosition;
                }
                else
                {
                    firstPosition.vector = g.transform.localPosition;
                }
            }

            if (lastPosition.assignCurrent)
            {
                lastPosition.assignCurrent = false;
                if (g.GetComponent<RectTransform>())
                {
                    lastPosition.vector = g.GetComponent<RectTransform>().localPosition;
                }
                else
                {
                    lastPosition.vector = g.transform.localPosition;
                }
            }

            if (attach || Application.isPlaying)
            {
                if (g.GetComponent<RectTransform>())
                {
                    g.GetComponent<RectTransform>().localPosition = (firstPosition.vector + ((lastPosition.vector - firstPosition.vector) * f));
                }
                else
                {
                    g.transform.localPosition = (firstPosition.vector + ((lastPosition.vector - firstPosition.vector) * f));
                }
            }

            
        }
    }

    internal override void OnEnableAnimation(PPTAnimation a)
    {
        if (a.attachToValue || Application.isPlaying)
            if (a.inverse)
            {
                if (!lastPosition.useObjectVector)
                    a.gameObjectForAnimation.transform.localPosition = (lastPosition.vector);
                else
                    lastPosition.vector = a.gameObjectForAnimation.transform.localPosition;
            }
            else {
                if (!firstPosition.useObjectVector)
                    a.gameObjectForAnimation.transform.localPosition = (firstPosition.vector);
                else
                    firstPosition.vector = a.gameObjectForAnimation.transform.localPosition;
            }
    }
}
