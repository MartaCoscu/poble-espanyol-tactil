﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class PPTPathStatus : PPTModule
{
//    public BezierCurve beizerPath;

    public bool pathOrientation;
    public bool changePathOrientation;

    private Vector3 point;
    private Vector3 point1;

    public override void OnCompleteAnimation()
    {
        //throw new NotImplementedException();
    }

    public override void Update(PPTAnimation g1, float f)
    {

        bool attach = g1.attachToValue;
        GameObject g = g1.gameObjectForAnimation;

        if (attach || Application.isPlaying)
        {
            f = f > 0.9999f ? 0.99f : f;
//            point = beizerPath.GetPointAt(f);
            g.transform.position = point;

            if (pathOrientation)
            {
                float incr = (changePathOrientation ? -0.0001f : 0.0001f);

                incr = f + incr < 0 ? 0 : (f + incr > 0.9999f ? 0.999f : f + incr);
                //Debug.Log("incr > " + incr);
 //               point1 = beizerPath.GetPointAt( incr);

                g.transform.LookAt(point1, g.transform.up);
            }
        }

    }

    internal override void OnEnableAnimation(PPTAnimation a)
    {
        /*if (a.attachToValue || Application.isPlaying)
            if (a.inverse)
            {
                if (!lastScale.useObjectVector)
                    a.gameObjectForAnimation.transform.localScale = (lastScale.vector);
                else
                    lastScale.vector = a.gameObjectForAnimation.transform.localScale;
            }
            else {
                if (!firstScale.useObjectVector)
                    a.gameObjectForAnimation.transform.localScale = (firstScale.vector);
                else
                    firstScale.vector = a.gameObjectForAnimation.transform.localScale;
            }


        if ((a.attachToValue || Application.isPlaying))
            if (a.inverse)
            {
                if (!lastScale.useObjectVector)
                    a.gameObjectForAnimation.transform.localScale = (lastScale.vector);
                else
                    lastScale.vector = a.gameObjectForAnimation.transform.localScale;
            }
            else {
                if (!firstScale.useObjectVector)
                    a.gameObjectForAnimation.transform.localScale = (firstScale.vector);
                else
                    firstScale.vector = a.gameObjectForAnimation.transform.localScale;
            }*/
    }
}
