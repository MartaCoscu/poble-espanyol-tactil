﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using RenderHeads.Media.AVProVideo;

[Serializable]
public class PPTAlphaModule : PPTModule
{
    [SerializeField]
    public bool concatenateEvent = false;
    [SerializeField]
    public FloatAssign init;
    [SerializeField]
    public FloatAssign fi;


    public override void OnCompleteAnimation()
    {
        //OnCompleteAnimation
    }

    public override void Update(PPTAnimation g1, float f)
    {
        {
            bool attach = g1.attachToValue;
            GameObject g = g1.gameObjectForAnimation;

            if (init.assignCurrent)
            {
                init.assignCurrent = false;

                init.floatValue = GetAlpha(g);
            }

            if (fi.assignCurrent)
            {
                fi.assignCurrent = false;
                fi.floatValue = GetAlpha(g);
            }

            if (attach || Application.isPlaying)
            {
                ChangeAlpha(g, init.floatValue + ((fi.floatValue - init.floatValue) * f));

            }
        }
    }

    internal override void OnEnableAnimation(PPTAnimation a)
    {
        if (a.attachToValue || Application.isPlaying)
            if (a.inverse)
            {
                if (!fi.useObjectVector)
                    ChangeAlpha(a.gameObjectForAnimation, fi.floatValue);
                //a.gameObjectForAnimation.transform.localPosition = (lastPosition.vector);
                else
                    fi.floatValue = GetAlpha(a.gameObjectForAnimation);//.transform.localPosition;
            }
            else {
                if (!init.useObjectVector)
                    ChangeAlpha(a.gameObjectForAnimation, init.floatValue);// a.gameObjectForAnimation.transform.localPosition = (firstPosition.vector);
                else
                    init.floatValue = GetAlpha(a.gameObjectForAnimation); //firstPosition.vector = a.gameObjectForAnimation.transform.localPosition;
            }
    }

    private float GetAlpha(GameObject gameObject)
    {
        //comencem
        if (gameObject.GetComponent<CanvasGroup>())
        {
            return gameObject.GetComponent<CanvasGroup>().alpha;
        }
        else
       if (gameObject.GetComponent<Text>())
        {
            Color c = gameObject.GetComponent<Text>().color;
            return c.a;// = f;
        }
        else
       if (gameObject.GetComponent<MeshRenderer>())
        {
            Color c = gameObject.GetComponent<MeshRenderer>().material.color;
            return c.a;// = f;
        }
        else
       if (gameObject.GetComponent<Image>())
        {

            if (gameObject.GetComponent<Image>().material)
            {
                return gameObject.GetComponent<Image>().material.GetColor("_Color").a;
            }
            else {
                Color c = gameObject.GetComponent<Image>().color;
                return c.a;// = f;
            }
        }
        else
       if (gameObject.GetComponent<RawImage>())
        {
            if (gameObject.GetComponent<Image>().material)
            {
                return gameObject.GetComponent<RawImage>().material.GetColor("_Color").a;
            }
            else {
                Color c = gameObject.GetComponent<RawImage>().color;
                return c.a;// = f;
            }
        }
/*        else
       if (gameObject.GetComponent<DisplayUGUI>())
        {

            Color c = gameObject.GetComponent<DisplayUGUI>().color;
            return c.a;// = f;
        }*/
        else
       if (gameObject.GetComponent<LineRenderer>())
        {

            Color c = gameObject.GetComponent<LineRenderer>().material.GetColor("_TintColor");
            return c.a;// = f;
        }
        else
       if (gameObject.GetComponent<Material>())
        {

            Color c = gameObject.GetComponent<LineRenderer>().material.GetColor("_TintColor");
            return c.a;// = f;
        }
        return 0;
    }

    private void ChangeAlpha(GameObject gameObject, float f)
    {

        if (concatenateEvent) {
            foreach (Transform t in gameObject.transform) {
                ChangeAlpha(t.gameObject, f);
            }
        }
        //comencem
        if (gameObject.GetComponent<CanvasGroup>())
        {
            gameObject.GetComponent<CanvasGroup>().alpha = f;
            return;
        }
        else
        if (gameObject.GetComponent<Text>())
        {
            Color c = gameObject.GetComponent<Text>().color;
            c.a = f;
            gameObject.GetComponent<Text>().color = c;
            return;

        }
        else

        if (gameObject.GetComponent<MeshRenderer>())
        {
            Color c = gameObject.GetComponent<MeshRenderer>().material.color;
            c.a = f;
            gameObject.GetComponent<MeshRenderer>().material.color = c;
            return;
        }
        else
        if (gameObject.GetComponent<Image>())
        {
            if (gameObject.GetComponent<Image>().material)
            {
                Color c = gameObject.GetComponent<Image>().material.GetColor("_Color");
                c.a = f;
                gameObject.GetComponent<Image>().material.SetColor("_Color", c);
            }
            else {
                Color c = gameObject.GetComponent<Image>().color;
                c.a = f;
                gameObject.GetComponent<Image>().color = c;
            }
            return;
        }
        else
        if (gameObject.GetComponent<RawImage>())
        {

            if (gameObject.GetComponent<RawImage>().material)
            {
                Color c = gameObject.GetComponent<RawImage>().material.GetColor("_Color");
                c.a = f;
                gameObject.GetComponent<RawImage>().material.SetColor("_Color", c);
            }
            else {
                Color c = gameObject.GetComponent<RawImage>().color;
                c.a = f;
                gameObject.GetComponent<RawImage>().color = c;
            }


           
            return;
        }
        else
/*        if (gameObject.GetComponent<DisplayUGUI>())
        {

            Color c = gameObject.GetComponent<DisplayUGUI>().color;
            c.a = f;
            gameObject.GetComponent<DisplayUGUI>().color = c;
            return;
        }
        else*/

        if (gameObject.GetComponent<LineRenderer>())
        {

            Color c = gameObject.GetComponent<LineRenderer>().material.GetColor("_TintColor");
            c.a = f;
            gameObject.GetComponent<LineRenderer>().material.SetColor("_TintColor", c);
            return;
        }
    }
}
