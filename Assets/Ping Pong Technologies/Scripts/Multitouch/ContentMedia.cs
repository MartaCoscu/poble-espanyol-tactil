﻿using System;
using System.IO;
using UnityEngine;

abstract public class ContentMedia : MonoBehaviour
{
    public delegate void OnContentLoaded();
    public OnContentLoaded onContentLoaded;

    public delegate void OnContentUnLoaded();
    public OnContentUnLoaded onContentUnLoaded;

    public abstract void LoadContent(FileInfo file, string resourcesPath);
    public abstract void UnLoadContent(string url);

    public abstract void Close();
}