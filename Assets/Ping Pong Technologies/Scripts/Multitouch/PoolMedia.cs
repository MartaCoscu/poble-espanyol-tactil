﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class PoolMedia : MonoBehaviour
{

    public string mediaFolder = "01_bt_outdoor/0";
    public bool loadMedia = false;

    public Vector2 range = new Vector2(25, -29);

    public float delayAnimatoinBetwen = 0.2f;

    public Transform rotationParent;
    public float timaAnimation=1.5f;

    //prefabs per cada component
    public GameObject prefabImage;
    public GameObject prefabVideo;
    private int mediaContent;

    // Update is called once per frame
    void Update()
    {
        if (loadMedia)
        {
            loadMedia = false;
            Debug.Log("Lets go to load content...");
            //en cas de tenir ja fotos... descarreguem l'arbre
            UnloadMediaContent();
            Debug.Log("Unloaded content");


            //anem a carregar cada element en funció del sistema que hi ha en media / streaming assets...
            DirectoryInfo infoJPEG = new DirectoryInfo(Application.streamingAssetsPath + "/media/" + mediaFolder+ "/JPEG/");
            DirectoryInfo infoVideos = new DirectoryInfo(Application.streamingAssetsPath + "/media/" + mediaFolder + "/VIDEO/");
            Debug.Log("dir file : " + Application.streamingAssetsPath + "/media/" + mediaFolder + "/JPEG/");
            Debug.Log("dir file : " + Application.streamingAssetsPath + "/media/" + mediaFolder + "/VIDEO/");

            //agafem la informació de cada fitxer
            FileInfo[] fileImages = infoJPEG.GetFiles();


            

            //fiquem contador de media a 0...
            mediaContent = 0;

            foreach (FileInfo file in fileImages)
            {
                GameObject g = null;

                Debug.Log("Fitxer : " + file.Name+" extension : "+ file.Extension);
                //només si son png o wmv
                switch (file.Extension) {
                    case ".png":
                    case ".jpg":
                        //instanciem cada una de les imatges...
                        Debug.Log("png / jpg file to be load!");



                        //mirem que no sigui un vídeo...
                        bool esVideo = false;
                        if (infoVideos.Exists)
                        {
                            FileInfo[] fileVideos = infoVideos.GetFiles();

                            foreach (FileInfo file2 in fileVideos)
                            {
                                if (Path.GetFileNameWithoutExtension(file.Name).CompareTo(Path.GetFileNameWithoutExtension(file2.Name)) == 0)
                                {
                                    //match amb el nom del fitxer... es un puto vídeo!!
                                    esVideo = true;
                                    break;
                                }
                            }
                        }


                        if (!esVideo)
                        {
                            g = Instantiate<GameObject>(prefabImage);
                        }
                        else{
                            g = Instantiate<GameObject>(prefabVideo);
                        }
                        
                        break;
                }

                if (g != null)
                {
                    g.name = file.Name;

                    //hi ha media a carregar...
                    mediaContent++;
                    PPT.Identity(g.transform, rotationParent);

                    //anem a carregar el seu correponent media
                    g.GetComponent<ContentMedia>().onContentLoaded = OnMediaContentLoaded;
                    Debug.Log("Loading : " + Application.streamingAssetsPath + "/media/" + mediaFolder + "/JPEG/" + file.Name);
                    g.GetComponent<ContentMedia>().LoadContent(file, "media/" + mediaFolder + "/JPEG/" + Path.GetFileNameWithoutExtension(file.Name));

                }
            }

            

        }
    }

    private void OnMediaContentLoaded()
    {
        //un contingut carregat
        mediaContent--;

        //en cas d'estar a 0 els continguts carregats... tot loaded!
        //lets go to animation!
        if (mediaContent == 0) {
            LoadContentMediaAnimation();
        }
    }

    private void LoadContentMediaAnimation()
    {
        //agafem les fotos que tenim de moment:
        int childsToBeLoad = rotationParent.childCount;

        //mirem els graus a fer servir:
        //+25 <--------> -29
        float lengthAngles = Mathf.Abs(range.x) + Mathf.Abs(range.y);
        float increment = lengthAngles / childsToBeLoad;

        int iteracions = 0;
        foreach (Transform t in rotationParent)
        {
            GameObject r = t.gameObject;
            //comencem el show amb les rotacions...
            t.localRotation = Quaternion.Euler(new Vector3(0, 0, range.x - (iteracions * increment)));


            //comença l'animació
            LeanTween.value(r, (float e) => {
                float posicioFinal = e * 833;
                float posicioActual = r.transform.localPosition.y;
                float deltaPosicio = posicioFinal - posicioActual;

                r.transform.Translate(new Vector3(0, deltaPosicio, 0));
            }, 0, 1, timaAnimation)
                .setDelay(iteracions * delayAnimatoinBetwen)
                .setEase(LeanTweenType.easeInOutBack);
            iteracions++;
        }
    }

    public  void UnloadMediaContent()
    {
        foreach (Transform t in rotationParent)
        {

            //desactivem colliders...
            t.GetComponent<BoxCollider>().enabled = false;
            //per cada objecte, enviem una funció de tancar
            t.GetComponent<ContentMedia>().Close();
        }
    }
}
