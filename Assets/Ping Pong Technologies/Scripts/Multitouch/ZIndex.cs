﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ZIndex : MonoBehaviour
{

   public  List<GameObject> elementsOberts = new List<GameObject>();

    public float espaiEntreCapes = 2;

    public void Add(GameObject element)
    { //Afegeix gameobject a la llista
        //Primer el traiem de la llista, i despres el tornem a posar on top
        Remove(element);
        elementsOberts.Add(element);
        Reload();
    }

    void Reload()
    {//Recarrega les posicions de les fotos
        for (int i = 0; i < elementsOberts.Count; i++)
        {
            elementsOberts[i].transform.position = new Vector3(elementsOberts[i].transform.position.x,
                 //Atencio: 10 es el valor per defecte de la prof. de les fitxes. la primera oberta sempre ha d'estar al 10. el 10 s'hauria d posar en varible 
                ( i)* espaiEntreCapes,
                
                //elementsOberts[i].transform.position.y,
               elementsOberts[i].transform.position.z
                );
            elementsOberts[i].GetComponent<RectTransform>().SetAsLastSibling();
        }

    }

    public void Remove(GameObject fitxa)
    { //borra gameobject de la llista
      //Comprovem, abans de treure l'element d la llista, que realment aquell element hi es (xq en tancar fitxa llancem aquesta accio per a tots els fills de la fitxa)
        if (elementsOberts.Contains(fitxa))
        {
            elementsOberts.Remove(fitxa);
        }
    }
}
