﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.IO;
using RenderHeads.Media.AVProVideo;

public class VideoContentMedia : ContentMedia
{
    public float initialScale = 0.4f;


 //  public MediaPlayer movieInstance;

    public override void LoadContent(FileInfo url, string resourcesPath)
    {
        //Anem a carregar el contingut media.
        StartCoroutine(LoadContentImage(url.FullName, resourcesPath));

        //anem a buscar els vídeos
        //agafem el pare
        DirectoryInfo r = new DirectoryInfo(url.Directory.Parent.FullName + "/VIDEO");
        LoadContentVideo(r.FullName, Path.GetFileNameWithoutExtension(url.Name));
    }

    private void LoadContentVideo(string folder, string name)
    {

        Debug.Log("LoadContentVideo : folder -> " + folder);
        Debug.Log("LoadContentVideo : name -> " + name);

     //  movieInstance.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, = true;
   //     movieInstance._folder=folder;
    //    movieInstance._filename = name+".wmv";
    //    movieInstance.LoadMovie(false);


    }
    private IEnumerator LoadContentImage(string url, string resources)
    {
        //PEr a la imatge passada:

        Sprite newSprite = Resources.Load<Sprite>(resources);

        if (newSprite) {
            GetComponent<Image>().sprite = newSprite;
        }
        else {

            WWW www = new WWW("file://" + url);
            float elapsedTime = 0.0f;

            while (!www.isDone)
            {
                elapsedTime += Time.deltaTime;
                if (elapsedTime >= 10.0f) break;
                yield return null;
            }

            if (!www.isDone || !string.IsNullOrEmpty(www.error))
            {
                Debug.LogError("Load Failed Media Image : " + url);
                yield break;
            }

            Texture2D texTmp = new Texture2D(1, 1);
            yield return null;

            www.LoadImageIntoTexture(texTmp);
            yield return null;

            //carreguem la textura a on sigui necessari...
            GetComponent<Image>().sprite = Sprite.Create(texTmp, new Rect(0, 0, texTmp.width, texTmp.height), new Vector2(0.5f, 0.5f));
        }

        int w = GetComponent<Image>().sprite.texture.width;
        int h = GetComponent<Image>().sprite.texture.height;

        //NATIVE!!!
        GetComponent<Image>().SetNativeSize();

        //verifiquem colliders...
        if (GetComponent<BoxCollider>())
        {
            GetComponent<BoxCollider>().size = new Vector3(w, h, 1);
        }

        //fiquem a escala inicial....
        transform.localScale = Vector3.one * initialScale;

        //i una vegada carregada la imatge...
        if (onContentLoaded != null)
            onContentLoaded();

    }

    public override void UnLoadContent(string url)
    {

    }

    public override void Close()
    {
        Destroy(gameObject);
    }
}
