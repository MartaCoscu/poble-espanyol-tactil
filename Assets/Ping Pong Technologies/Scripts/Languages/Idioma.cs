﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

[XmlRoot]
public class Idioma
{
    [XmlElement]
    public string id = "";
    [XmlElement]
    public string active_image = "";
    [XmlElement]
    public string bandera = "";
    [XmlElement]
    public string deactive_image = "";
    [XmlElement]
	public bool RTL = false;
    [XmlElement]
    public string description = "";

 
}