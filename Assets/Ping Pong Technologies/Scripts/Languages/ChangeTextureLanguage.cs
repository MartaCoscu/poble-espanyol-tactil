﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChangeTextureLanguage : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        Idiomes.onChangeIdioma += OnChangeIdioma;
    }

    public void OnChangeIdioma()
    {
        if (GetComponent<Image>()) {
            //agafem el botó corresponent a lo que toca...
            string nameResources = gameObject.name;
            nameResources = Idiomes.GetCurrentIdioma().id + "/" + nameResources;

            //el carreguem en l'imatge...
            GetComponent<Image>().sprite = Resources.Load<Sprite>(nameResources);

        }

        if (GetComponent<Button>())
        {
            //agafem el botó corresponent a lo que toca...
            string nameResources = gameObject.name+"-actiu";
            nameResources = Idiomes.GetCurrentIdioma().id + "/" + nameResources;

            //el carreguem en l'imatge...
            SpriteState st = new SpriteState();
            
            st.disabledSprite =  null;
            st.highlightedSprite = null;
            st.pressedSprite = Resources.Load<Sprite>(nameResources);

            Debug.Log("nameResources << " + nameResources);

            GetComponent<Button>().spriteState = st;


        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnDisable()
    {
        Idiomes.onChangeIdioma -= OnChangeIdioma;
    }

    public void OnDestroy()
    {
        Idiomes.onChangeIdioma -= OnChangeIdioma;
    }
}
