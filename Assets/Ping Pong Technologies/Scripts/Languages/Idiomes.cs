﻿
using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

[XmlRoot("idiomes")]
public class Idiomes
{
    [XmlArray(ElementName = "idiomes")]
    [XmlArrayItem(ElementName = "idioma")]
    public List<Idioma> idiomes = new List<Idioma>();


    public delegate void OnChangeIdioma();
    public static OnChangeIdioma onChangeIdioma;

    internal static void SetCurrentIdioma(string nameIdioma)
    {
        foreach (Idioma i in GetInstance().idiomes)
        {
            if (i.description.CompareTo(nameIdioma) == 0)
            {
                SetCurrentIdioma(i);
            }
        }

    }

    private static Idiomes internalIdiomes;
    private Idioma currentIdioma = null;

    public static Idiomes GetInstance()
    {
        //carreguem les preguntes que toquen...
        if (internalIdiomes == null)
        {
            internalIdiomes = PPT.LoadSerializer<Idiomes>(Application.streamingAssetsPath + "/idiomes.xml");
        }

        if (internalIdiomes == null)
        {
            internalIdiomes = new Idiomes();
        }
        return internalIdiomes;
    }

    public static void SetCurrentIdioma(Idioma idioma)
    {
        GetInstance().currentIdioma = idioma;
        if (onChangeIdioma != null)
            onChangeIdioma();
    }


    public static Idioma GetCurrentIdioma()
    {
        if (GetInstance().currentIdioma == null)
        {
            bool firstSelected = false;
            foreach (Idioma idioma in Idiomes.GetInstance().idiomes)
            {
                if (!firstSelected)
                {
                    firstSelected = true;
                    GetInstance().currentIdioma = idioma;
                }
            }
        }
        return GetInstance().currentIdioma;
    }
}