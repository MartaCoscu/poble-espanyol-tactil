﻿using System;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
[RequireComponent(typeof(CanvasGroup))]
public class MenuMosaic : MonoBehaviour
{
    public GameObject activate;
    public GameObject deactivate;
    public GameObject activate_selected;

    public void LoadOption(int id)
    {
        foreach (Transform t in transform)
        {

            switch (t.gameObject.name)
            {
                case "deactive":
                    deactivate = t.gameObject;
                    break;
                case "activate_selected":
                    activate_selected = t.gameObject;
                    break;
                case "activate":
                    activate = t.gameObject;
                    break;

            }
        }

        //anem a carregar el seu contingut...
        Sprite s = Resources.Load<Sprite>("0/1/" + id + "/deactive");
        if (s != null)
        {
            if (!deactivate.GetComponent<RawImage>())
            {
                deactivate.AddComponent<RawImage>();
            }
            deactivate.GetComponent<RawImage>().texture = s.texture;
        }
        //anem a carregar el seu contingut...
        s = Resources.Load<Sprite>("0/1/" + id + "/activate");
        if (s != null)
        {
            if (!activate.GetComponent<RawImage>())
            {
                activate.AddComponent<RawImage>();
            }
            activate.GetComponent<RawImage>().texture = s.texture;
        }
        //anem a carregar el seu contingut...
        s = Resources.Load<Sprite>("0/1/" + id + "/activate_selected");
        if (s != null)
        {
            if (!activate_selected.GetComponent<RawImage>())
            {
                activate_selected.AddComponent<RawImage>();
            }
            activate_selected.GetComponent<RawImage>().texture = s.texture;
        }


        deactivate.GetComponent<RectTransform>().SetSiblingIndex(0);
        activate_selected.GetComponent<RectTransform>().SetSiblingIndex(1);
        activate.GetComponent<RectTransform>().SetSiblingIndex(2);

    }

    internal void ActivateSelected()
    {
        activate_selected.SetActive(true);

        if (!activate_selected.GetComponent<CanvasGroup>())
        {
            activate_selected.AddComponent<CanvasGroup>();
        }

        activate_selected.GetComponent<CanvasGroup>().alpha = 0;
        LeanTween.alphaCanvas(activate_selected.GetComponent<CanvasGroup>(), 1, 0.5f).setOnComplete(() =>
        {
            deactivate.SetActive(false);
        }); ;

        activate.SetActive(false);

        GetComponent<CanvasGroup>().interactable = true;
    }


    internal void Deactivate()
    {
        GetComponent<CanvasGroup>().interactable = false;
        GetComponent<CanvasGroup>().alpha = 1;
        activate.SetActive(false);
        deactivate.SetActive(true);
        activate_selected.SetActive(false);
    }

    internal void Activate()
    {
        deactivate.SetActive(false);

        activate.SetActive(true);



        if (!activate.GetComponent<CanvasGroup>())
        {
            activate.AddComponent<CanvasGroup>();
        }

        activate.GetComponent<CanvasGroup>().alpha = 0;
        LeanTween.alphaCanvas(activate.GetComponent<CanvasGroup>(), 1, 0.5f).setOnComplete(() =>
        {
            activate_selected.SetActive(false);
        }); ;

        GetComponent<CanvasGroup>().interactable = true;
    }
}