﻿using UnityEngine;
using System.Collections;


public class MosaicAutoplay : MonoBehaviour {

	
	// Update is called once per frame
	void Update () {
        if (GetComponent<MosaicGlobal>().GetActualMenu() == 0)
        {
            GetComponent<MosaicGlobal>().autoplay = true;
        }

        if (GetComponent<MosaicGlobal>().GetActualMenu() >0)
        {
            GetComponent<MosaicGlobal>().autoplay = false;
        }
    }
}
