﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class MenuControllerMosaic : MonoBehaviour
{

    public MosaicGlobal mosaic;

    public int currentOption = 0;
    public int currentOption2 = 0;

    [SerializeField]
    public MenuMosaic[] options;

    //controlador per poder "jugar" amb el mosaic de economia...
    public void OnEnable()
    {
        
        //carreguem opcions...
        int id = 0;
        foreach (MenuMosaic m in options)
        {
            m.LoadOption(id);
            m.Deactivate();
            id++;
        }

        ActiveLayer(0, 0 + 1);
    }


    public void ActiveLayer(int c, int c2)
    {
        if (c != 0)
        {
            //deixem activat l'anterior...
            options[currentOption].Activate();
            options[currentOption2].Activate();


        }

        //activem la nova opció
        options[c].ActivateSelected();
        options[c2].ActivateSelected();

        currentOption = c;
        currentOption2 = c2;

    }
    public void NextSlide()
    {
        //deixem activat l'anterior...
        options[currentOption].Activate();
        options[currentOption2].Activate();

        //activem la nova opció
        options[currentOption + 2].ActivateSelected();
        options[currentOption2 + 2].ActivateSelected();

        currentOption += 2;
        currentOption2 += 2;

    }
}
