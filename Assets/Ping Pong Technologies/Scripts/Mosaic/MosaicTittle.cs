﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MosaicTittle : MonoBehaviour {

    public Text text;
    public string[] titles;

	
	// Update is called once per frame
	void Update () {
        if (GetComponent<MosaicGlobal>().GetActualMenu() == 0)
        {
            text.text = titles[0];
        }

        if (GetComponent<MosaicGlobal>().GetActualMenu() ==1)
        {
            text.text = titles[1];
        }
    }
}
