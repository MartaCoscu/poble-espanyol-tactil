﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;


[Serializable]
public class Ubication
{
    public int x, y, w, h;
}

[Serializable]
public class MosaicMenu
{
    public MosaicAnimation[] mosaicsAnimation;
}

[Serializable]
public class MosaicUnit
{
    public int provability;
    public Ubication ubication = null;
    public MosaicAnimation mosaic;
    public bool active;
    public MosaicDefinitionStruct definition = new MosaicDefinitionStruct();

}

[Serializable]
public class MosaicDefinitionStruct
{
    public MosaicAnimationTypeOut mosaicIn;
    public MosaicAnimationTypeIn mosaicOut;

    public bool makeIn;
    public bool makeOut;
    public bool stay;

    //public bool active;
    public Texture2D texture;
    public MosaicAnimation mosaic;


}

[Serializable]
public class MosaicDefinition
{
    public MosaicDefinitionStruct[] definition;
}

[Serializable]
public class TextureMosaic
{
    public String reference;
    public List<TextureElement> texturesElements = new List<TextureElement>();

}


[Serializable]
public class TextureElement
{
    public Texture2D texture;
    public bool gived = false;
}


public class MosaicGlobal : MonoBehaviour
{

    public bool launchAnimation = false;
    public bool randomMosaic;
    public bool autoplay = true;

    public int graellaWidth = 1;
    public int graellaHeight = 1;

    public LeanTweenType typeAnimationIn;
    public LeanTweenType typeAnimationOut;

    public float timeIn = 0.8f;
    public float timeOut = 0.8f;

    //private List<MosaicAnimation> mosaics = new List<MosaicAnimation>();
    public delegate void OnLoadedmosaic();
    public static OnLoadedmosaic onLoadMosaic;

    // public int actualListMosaic = 0;
    //definim en cas de no random
    // public MosaicDefinition[] mosaicDefinition;

    //mosaic que tenim en cas de random
    public MosaicUnit[] mosaics;

    //mosaics que fem servir per fer menus...
    public MosaicMenu[] menuMosaics;
    public int actualMenu = -1;
    private int tmpActualMenu = -1;

    public void SetActualMenu(int i)
    {
        actualMenu = i;
    }


    public int GetActualMenu()
    {
        return actualMenu;
    }
    private List<MosaicUnit> mosaicsForced = new List<MosaicUnit>();


    private List<MosaicUnit> mosaicsPool = new List<MosaicUnit>();

    private List<MosaicUnit> mosaicsActuals = new List<MosaicUnit>();

    private int currentElements = 0;
    public string mosaicFolder = "Mosaics";


    public int timeToAnimation = 5;

    public int[][] zones;
    public int[][] zonesMortes;

    public List<TextureMosaic> textures = new List<TextureMosaic>();
    private bool primeraPassada;
    private LTDescr anim;
    public bool playing;

    public void Load()
    {

        //no està en animació...
        playing = false;

        //activem primera passada
        primeraPassada = true;

        //reiniciem textures..
        textures = new List<TextureMosaic>();

        //inicialitzem graella zones
        zones = new int[graellaWidth][];
        for (int i = 0; i < graellaWidth; i++)
        {
            zones[i] = new int[graellaHeight];
            for (int e = 0; e < graellaHeight; e++)
            {
                zones[i][e] = 0;
            }
        }
        //inicialitzem graella zonesMortes
        zonesMortes = new int[graellaWidth][];
        for (int i = 0; i < graellaWidth; i++)
        {
            zonesMortes[i] = new int[graellaHeight];
            for (int e = 0; e < graellaHeight; e++)
            {
                zonesMortes[i][e] = 0;
            }
        }


        //carreguem en memoria le stextures...
        //anem per definicions
        /*int idStreamingAssetsFolder = 0;
        foreach (MosaicDefinition e in mosaicDefinition)
        {
            //Debug.Log("Entren en carrega de mosaics...");
            int idElement = 0;
            for (int w = 0; w < e.definition.Length; w++)
            {// (MosaicDefinitionStruct d in e.definition)
             //carrequem la textura per cada deifnició donada pels definitions mosaics
                int tmp = w;
                //Debug.Log("Path << "+ "file://" + Application.streamingAssetsPath + "/Mosaics/" + idStreamingAssetsFolder + "/" + idElement + ".png");
                e.definition[tmp].texture = null;

                //desactivem tots els mosaics
                //e.definition[tmp].mosaic.gameObject.SetActive(false);

                String url = "file://" + Application.streamingAssetsPath + "/" + mosaicFolder + "/" + idStreamingAssetsFolder + "/" + e.definition[tmp].mosaic.gameObject.name + ".png";
                Debug.Log("url << " + url);
                WWW www = new WWW(url);
                float elapsedTime = 0.0f;

                if (www.isDone || string.IsNullOrEmpty(www.error))
                {
                    Texture2D texTmp = new Texture2D(128, 128);
                    www.LoadImageIntoTexture(texTmp);

                    e.definition[tmp].texture = (Texture2D)texTmp;
                }
                idElement++;
            }
            idStreamingAssetsFolder++;
        }*/

        //anem a fer un pool d'imatges en mode random
        List<String> tipus = new List<string>();
        for (int i = 0; i < mosaics.Length; i++)
        {
            if (!tipus.Contains(mosaics[i].ubication.w + "x" + mosaics[i].ubication.h))
            {
                tipus.Add(mosaics[i].ubication.w + "x" + mosaics[i].ubication.h);
            }
        }

        //fem array de mosaics per tenir com pool
        foreach (MosaicUnit m in mosaics)
        {
            for (int i = 0; i < m.provability; i++)
            {
                mosaicsPool.Add(m);
            }
        }

        //per cada tipus de tenim...
        //recollim la seva textura...
        foreach (String s in tipus)
        {
            TextureMosaic t = null;
            foreach (TextureMosaic r in textures)
            {
                if (r.reference.CompareTo(s) == 0)
                {
                    t = r;
                    break;
                }
            }

            bool addTextureAsNew = false;

            if (t == null)
            {
                addTextureAsNew = true;
                t = new TextureMosaic();
            }

            t.reference = s;
            try
            {
                String path = Application.streamingAssetsPath + "/" + mosaicFolder + "/" + t.reference;
                foreach (String f in Directory.GetFiles(path))
                {
                    if (Path.GetExtension(f).CompareTo(".png") == 0)
                    {
                        String url = "file://" + f;

                        Debug.Log("url << " + url);
                        WWW www = new WWW(url);
                        float elapsedTime = 0.0f;

                        if (www.isDone || string.IsNullOrEmpty(www.error))
                        {
                            Texture2D texTmp = new Texture2D(128, 128);
                            www.LoadImageIntoTexture(texTmp);

                            TextureElement n = new TextureElement();
                            n.texture = texTmp;
                            n.gived = false;

                            t.texturesElements.Add(n);
                        }
                    }
                }
                if (addTextureAsNew)
                    textures.Add(t);
            }
            catch (Exception e)
            {
                Debug.LogError("Exception: " + e.Message);
            }
        }

    }

    public void Awake()
    {
        Load();
    }

    public void LaunchMosaic(float delayed)
    {
        Debug.Log("StartAnimation << " + delayed);
        if (delayed == 0)
        {
            launchAnimation = true;
        }
        else {
            LeanTween.delayedCall(delayed, () =>
            {
                launchAnimation = true;
            });
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void Update()
    {
        if (launchAnimation && !playing)
        {
            playing = true;

            //Debug.Log("LaunchAnimation << " + launchAnimation);
            launchAnimation = false;


            //mode play en llistat de mosaics ja generats...
            if (autoplay)
            {
                if (anim != null)
                    LeanTween.cancel(anim.id);

                anim = LeanTween.delayedCall(timeToAnimation, () =>
                    {
                        if (autoplay)
                        {
                            //   actualListMosaic++;
                            //Debug.Log("Entream amb actualList << " + actualListMosaic);
                            launchAnimation = true;
                            // if (actualListMosaic >= mosaicDefinition.Length)
                            {
                                //    actualListMosaic = 0;
                            }
                        }

                    });
            }

            if (randomMosaic)
            {
                //inicialitzem graella de posicions
                for (int x = 0; x < graellaWidth; x++)
                {
                    for (int y = 0; y < graellaHeight; y++)
                    {
                        zones[x][y] = 0;
                        //mirem zones mortes de la graella
                        if (zonesMortes[x][y] == 1)
                        {
                            zones[x][y] = 1;
                        }
                    }
                }

                if (actualMenu != tmpActualMenu)
                {
                    if (actualMenu >= 0 && actualMenu < menuMosaics.Length)
                    {

                        mosaicsForced = new List<MosaicUnit>();

                        foreach (MosaicAnimation ani in menuMosaics[actualMenu].mosaicsAnimation)
                        {
                            MosaicUnit newMosaicUnit = new MosaicUnit();
                            newMosaicUnit.ubication = new Ubication();
                            newMosaicUnit.ubication.x = int.Parse(ani.gameObject.name.Substring(2, 1));
                            newMosaicUnit.ubication.y = int.Parse(ani.gameObject.name.Substring(4, 1));
                            newMosaicUnit.ubication.w = int.Parse(ani.gameObject.name.Substring(6, 1));
                            newMosaicUnit.ubication.h = int.Parse(ani.gameObject.name.Substring(8, 1));

                            newMosaicUnit.mosaic = ani;


                            mosaicsForced.Add(newMosaicUnit);
                        }

                        for (int a = 0; a < mosaicsForced.Count; a++)
                        {
                            mosaicsForced[a].definition.makeOut = false;
                            mosaicsForced[a].definition.makeIn = false;
                            mosaicsForced[a].definition.stay = false;
                            mosaicsForced[a].mosaic.RestartElement();
                            mosaicsForced[a].active = false;
                        }
                    }
                    else {
                        actualMenu = -1;
                        mosaicsForced.Clear();
                        mosaicsForced = new List<MosaicUnit>();
                    }
                    tmpActualMenu = actualMenu;
                }


                //fem una copia dels elements que volerm ficar
                List<MosaicUnit> totsElsMosaics = new List<MosaicUnit>(mosaicsPool);

                //fem arraty per poder ficar nou element
                List<MosaicUnit> mosaicsACarregar = new List<MosaicUnit>();


                //afegim fills que si han d'estar...
                //fills forçats per construir un menú o quelcom altre cosa...
                foreach (MosaicUnit m in mosaicsForced)
                {
                    bool lliure = true;
                    for (int x = m.ubication.x; x < m.ubication.x + m.ubication.w; x++)
                    {
                        for (int y = m.ubication.y; y < m.ubication.y + m.ubication.h; y++)
                        {
                            if (zones[x][y] == 1)
                            {
                                lliure = false;
                                break;
                            }
                        }
                        if (!lliure)
                            break;
                    }

                    if (lliure)
                    {
                        //marquem les caselles ocupades...
                        for (int x = m.ubication.x; x < m.ubication.x + m.ubication.w; x++)
                        {
                            for (int y = m.ubication.y; y < m.ubication.y + m.ubication.h; y++)
                            {
                                zones[x][y] = 1;
                            }
                        }
                        mosaicsACarregar.Add(m);
                    }
                }

                //mentres hi hagi algun mosaic possible per ubicar....
                while (totsElsMosaics.Count > 0)
                {
                    //agafem un element a l'atzar
                    MosaicUnit m = totsElsMosaics[UnityEngine.Random.Range(0, totsElsMosaics.Count - 1)];

                    //mirem que en la graella estigui lliure...
                    if (zones[m.ubication.x][m.ubication.y] == 0)
                    {
                        //una vegada veiem que la pos està lliure...
                        //mirem la mida del mosaic que té...
                        if (m.ubication.w == 1 && m.ubication.h == 1)
                        {
                            //es un mosaic d'un... tot ok...
                            mosaicsACarregar.Add(m);
                            zones[m.ubication.x][m.ubication.y] = 1;
                            //l'eliminem del pool

                        }
                        else {
                            //mirem si la mida que hi ha del mosaic 
                            //cap en la graella actual...
                            bool lliure = true;
                            for (int x = m.ubication.x; x < m.ubication.x + m.ubication.w; x++)
                            {
                                for (int y = m.ubication.y; y < m.ubication.y + m.ubication.h; y++)
                                {
                                    if (zones[x][y] == 1)
                                    {
                                        lliure = false;
                                        break;
                                    }
                                }
                                if (!lliure)
                                    break;
                            }

                            if (lliure)
                            {
                                //marquem les caselles ocupades...
                                for (int x = m.ubication.x; x < m.ubication.x + m.ubication.w; x++)
                                {
                                    for (int y = m.ubication.y; y < m.ubication.y + m.ubication.h; y++)
                                    {
                                        zones[x][y] = 1;
                                    }
                                }
                                mosaicsACarregar.Add(m);

                            }
                        }
                    }

                    totsElsMosaics.Remove(m);
                }

                //mirem doncs:
                //en mosaicsToAdd tenim els mosaics que volem
                //en mosaicsActuals els que corren ara...

                //ho desactivem tot
                foreach (Transform t in transform)
                {
                    if (t.GetComponent<MosaicAnimation>())
                    {
                        t.gameObject.SetActive(false);
                    }
                }
                //també els mosaics...
                int i = 0;
                for (i = 0; i < mosaicsPool.Count; i++)
                {
                    mosaicsPool[i].active = false;
                    mosaicsPool[i].definition.makeIn = false;
                    mosaicsPool[i].definition.makeOut = false;
                }

                //

                List<MosaicUnit> llistaMosaicsPerActuar = new List<MosaicUnit>();

                //anem a intentar fer animcació...
                //mirem mosaics actuals
                //fiquem definició de que han de fer out
                for (i = 0; i < mosaicsActuals.Count; i++)
                {
                    //agafem el mosaic per actuar
                    MosaicUnit tmp = mosaicsActuals[i];

                    //en cas de no ser estàtic...
                    if (!mosaicsForced.Contains(tmp))
                    {
                        //fem que el fill no sigui estàtic
                        tmp.definition.stay = false;

                        //es defineix com un element de sortida
                        tmp.definition.makeOut = true;
                    }

                    //s'afageix en array de mosaics a actuar....
                    llistaMosaicsPerActuar.Add(tmp);
                }

                //borrem el llistat d'elements que tenim actualment...
                mosaicsActuals.Clear();
                mosaicsActuals = new List<MosaicUnit>();

                //mirem els que tenim per fer el makeIn...
                for (i = 0; i < mosaicsACarregar.Count; i++)
                {
                    //agafem el mosaic per actuar
                    MosaicUnit tmp = mosaicsACarregar[i];

                    //mirem si el contingut es forçat o no...
                    //per mirar això mirem si aquest ja ha fet el makeIn
                    //com el contingut forçat està "fora" de tot l'array de mosaics...
                    //aquest no es reseteja el seu makeIn a false...
                    if (mosaicsForced.Contains(tmp))
                    {
                        if (!tmp.active)
                        {
                            tmp.active = true;
                            tmp.definition.stay = true;
                            //fem que l'element sigui estàtic...
                            tmp.definition.makeIn = true;
                        }
                        else {
                            //ja ha estat activat
                            tmp.definition.stay = true;
                            tmp.definition.makeIn = false;
                        }
                    }
                    else {
                        //es definiex element com entrada
                        tmp.definition.makeIn = true;
                    }

                    if (llistaMosaicsPerActuar.Contains(tmp))
                    {
                        int key = llistaMosaicsPerActuar.IndexOf(tmp);
                        llistaMosaicsPerActuar[key] = tmp;
                    }
                    else {
                        llistaMosaicsPerActuar.Add(tmp);
                    }
                    mosaicsActuals.Add(tmp);
                    /*bool added = false;
                    for (int e = 0; e < llistaMosaicsPerActuar.Count; e++) {

                        if (llistaMosaicsPerActuar[e].id == mosaicsACarregar[i].id)
                        {
                            MosaicUnit t = llistaMosaicsPerActuar[e];
                            t.definition = tmp;
                            llistaMosaicsPerActuar[e] = t;
                            added = true;
                        }
                    }
                    if(!added) {
                        llistaMosaicsPerActuar.Add(w);
                    }*/
                }

                //reiniciem el tema de les textures... mirar de que no surtin repetides...
                RestartGivedTextures();

                //anem a intentar fer animcació...
                for (i = 0; i < llistaMosaicsPerActuar.Count; i++)
                {
                    //agafem el mosaic per actuar

                    //activem aquest mosaic 
                    llistaMosaicsPerActuar[i].active = true;

                    //agafem i assignem el seu objecte...
                    llistaMosaicsPerActuar[i].definition.mosaic = llistaMosaicsPerActuar[i].mosaic;

                    //activem el seu gameobject...
                    llistaMosaicsPerActuar[i].mosaic.gameObject.SetActive(true);

                    //em d'agafar imatges de referencia...
                    //mirem que no sigui un menu...
                    //en cas de ser un menú no em de fer res amb el tema càrrega de textures...
                    if (!llistaMosaicsPerActuar[i].mosaic.gameObject.name.StartsWith("m"))
                        llistaMosaicsPerActuar[i].definition.mosaic.SetTexture(GetTexture(llistaMosaicsPerActuar[i].ubication.w + "x" + llistaMosaicsPerActuar[i].ubication.h));

                    llistaMosaicsPerActuar[i].definition.mosaic.animIn = UnityEngine.Random.Range(0, ((int)MosaicAnimationTypeIn.right));
                    llistaMosaicsPerActuar[i].definition.mosaic.animOut = UnityEngine.Random.Range(0, ((int)MosaicAnimationTypeOut.right_left));

                    llistaMosaicsPerActuar[i].definition.mosaic.SetAnimationIn(typeAnimationIn);
                    llistaMosaicsPerActuar[i].definition.mosaic.SetAnimationOut(typeAnimationOut);

                    llistaMosaicsPerActuar[i].definition.mosaic.SetTimeIn(timeIn);
                    llistaMosaicsPerActuar[i].definition.mosaic.SetTimeOut(timeOut);

                    //en cas de fer timeIn
                    if (llistaMosaicsPerActuar[i].definition.makeIn)
                    {
                        llistaMosaicsPerActuar[i].definition.mosaic.SetDelayIn(timeOut + 0.1f);
                    }

                    llistaMosaicsPerActuar[i].definition.mosaic.StartAnimation(llistaMosaicsPerActuar[i].definition.makeIn, llistaMosaicsPerActuar[i].definition.makeOut, llistaMosaicsPerActuar[i].definition.stay);
                }

                //llencem events en cas de que hi hagin...
                if (onLoadMosaic != null)
                {
                    LeanTween.delayedCall((timeOut + 0.1f) * 2, () =>
                      {
                          onLoadMosaic();
                          playing = false;
                      });
                }
            }
            else {
                //NO RANDOM
                foreach (Transform t in transform)
                {
                    if (t.GetComponent<MosaicAnimation>())
                    {
                        t.gameObject.SetActive(false);
                    }
                }

                for (int i = 0; i < mosaics.Length; i++)
                {
                    mosaics[i].active = false;
                }

                //  MosaicDefinition m = mosaicDefinition[actualListMosaic];

                /*    int id = 0;
                    foreach (MosaicDefinitionStruct e in m.definition)
                    {
                        for (int i = 0; i < mosaics.Length; i++)
                        {
                            if (mosaics[i].mosaic == e.mosaic)
                            {
                                mosaics[i].active = true;
                                break;
                            }
                        }


                        e.mosaic.gameObject.SetActive(true);

                        e.mosaic.SetTexture(e.texture);

                        e.mosaic.animIn = (int)e.mosaicIn;
                        e.mosaic.animOut = (int)e.mosaicOut;

                        e.mosaic.SetAnimationIn(typeAnimationIn);
                        e.mosaic.SetAnimationOut(typeAnimationOut);

                        e.mosaic.SetTimeIn(timeIn);
                        e.mosaic.SetTimeOut(timeOut);

                        //en cas de fer timeIn
                        if (e.makeIn)
                        {
                            e.mosaic.SetDelayIn(timeOut + 0.1f);
                        }

                        e.mosaic.StartAnimation(e.makeIn, e.makeOut, e.stay);
                    }*/
            }
        }
    }

    private void RestartGivedTextures()
    {
        if (primeraPassada)
            primeraPassada = false;
        else
            primeraPassada = true;

        if (primeraPassada)
        {
            for (int e = 0; e < textures.Count; e++)
            {
                for (int i = 0; i < textures[e].texturesElements.Count; i++)
                {
                    textures[e].texturesElements[i].gived = false;
                }
            }
            //Debug.Log("Entrem a resetejar gived images");
        }
    }

    private Texture2D GetTexture(string v)
    {
        for (int e = 0; e < textures.Count; e++)
        {
            if (textures[e].reference.CompareTo(v) == 0)
            {
                //foreach (TextureElement n in t.texturesElements) {
                //while (true){
                for (int i = 0; i < textures[e].texturesElements.Count; i++)
                {

                    //elements random a mostrar
                    //int e = UnityEngine.Random.Range(0, t.texturesElements.Count - 1);
                    if (!textures[e].texturesElements[i].gived)
                    {
                        textures[e].texturesElements[i].gived = true;
                        return textures[e].texturesElements[i].texture;
                    }
                }

                Debug.Log("Entra en : " + v + " amb uns id de textures en e : " + e + " i amb un i ");

            }
        }
        Debug.LogError("Return a non valid texture fro reference : " + v);
        return null;
    }

    public void OnApplicationQuit()
    {
        textures.Clear();
    }

    private bool IsZonesEmpty(int[][] zones)
    {
        for (int x = 0; x < graellaWidth; x++)
        {
            for (int y = 0; y < graellaHeight; y++)
            {
                if (zones[x][y] == 0)
                    return true;
            }
        }
        return false;
    }
}
