﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;


public enum MosaicAnimationTypeOut
{
    up_down,
    down_up,
    left_right,
    right_left,
}
public enum MosaicAnimationTypeIn
{
    down,
    up,
    left,
    right,
}


public class MosaicAnimation : MonoBehaviour
{

    public List<PPTAnimation> animationEntrada = new List<PPTAnimation>();
    public List<PPTAnimation> animationSortida = new List<PPTAnimation>();

    public int animIn, animOut;

    public GameObject nextPhoto;
    public GameObject photo;
    private GameObject firstPhoto;

    public void Awake() {
        firstPhoto = photo;
    }

    public void Start()
    {
        nextPhoto.transform.localPosition = new Vector3(8000, 0, 0);
        photo.transform.localPosition = new Vector3(8000, 0, 0);


        //modifiquem imatges mides...
        //nextPhoto.GetComponent<RectTransform>().sizeDelta = GetComponent<RectTransform>().sizeDelta;
        //photo.GetComponent<RectTransform>().sizeDelta = GetComponent<RectTransform>().sizeDelta;

        foreach (Transform t in transform)
        {
            if (t.name.CompareTo("animation_in") == 0)
            {
                foreach (Transform e in t)
                {
                    animationEntrada.Add(e.GetComponent<PPTAnimation>());
                    SetSizeToAnimation(e.GetComponent<PPTAnimation>());

                }
            }
            if (t.name.CompareTo("animation_out") == 0)
            {
                foreach (Transform e in t)
                {
                    animationSortida.Add(e.GetComponent<PPTAnimation>());
                    SetSizeToAnimation(e.GetComponent<PPTAnimation>());
                }
            }
        }
    }

    private void SetSizeToAnimation(PPTAnimation pPTAnimation)
    {
        switch (pPTAnimation.gameObject.name)
        {
            case "up_down":
                pPTAnimation.modules.position.firstPosition.vector = new Vector3(0, GetComponent<RectTransform>().sizeDelta.y, 0);
                break;
            case "down_up":
                pPTAnimation.modules.position.firstPosition.vector = new Vector3(0, -GetComponent<RectTransform>().sizeDelta.y, 0);
                break;
            case "left_right":
                pPTAnimation.modules.position.firstPosition.vector = new Vector3(-GetComponent<RectTransform>().sizeDelta.x, 0, 0);

                break;
            case "right_left":
                pPTAnimation.modules.position.firstPosition.vector = new Vector3(GetComponent<RectTransform>().sizeDelta.x, 0, 0);

                break;

        }
    }

    internal void StartAnimation(bool makeIn, bool makeOut, bool stay)
    {

        if (!stay)
        {
            nextPhoto.transform.localPosition = new Vector3(1000, 0, 0);
            photo.transform.localPosition = new Vector3(1000, 0, 0);
        }

        //agafem animació aleatoria 
        PPTAnimation aIn = animationEntrada[animIn];
        PPTAnimation aOut = animationSortida[animOut];

        aIn.gameObjectForAnimation = nextPhoto;
        aOut.gameObjectForAnimation = photo;

        aIn.onCompleteAnimationAction = () =>
        {
            GameObject tmp = nextPhoto;
            nextPhoto = photo;
            photo = tmp;

        };

        if (makeOut)
        {
            aOut.LaunchAnimation();
        }
        /* else {
             aIn.delay = 0;
         }*/

        if (makeIn)
        {
            aIn.LaunchAnimation();
        }


        //photo.SetActive(!notWorking);
    }

    internal void SetAnimationIn(LeanTweenType typeAnimationOut)
    {
        animationEntrada[animIn].delay = 0;
        animationEntrada[animIn].easeType = typeAnimationOut;
    }

    internal void SetAnimationOut(LeanTweenType typeAnimationOut)
    {
        animationSortida[animOut].delay = 0;
        animationSortida[animOut].easeType = typeAnimationOut;
    }

    internal void SetTexture(Texture2D texture)
    {
        nextPhoto.GetComponent<RawImage>().texture = texture;
    }

    internal void SetTimeIn(float timeIn)
    {
        animationEntrada[animIn].time = timeIn;
    }

    internal void SetTimeOut(float timeOut)
    {

        animationSortida[animOut].time = timeOut;
    }
    internal void SetDelayIn(float timeIn)
    {
        animationEntrada[animIn].delay = timeIn;
    }

    internal void SetDelayOut(float timeOut)
    {

        animationSortida[animOut].delay = timeOut;
    }

    internal void RestartElement()
    {
        if (firstPhoto != photo) {
            nextPhoto = photo;
            photo = firstPhoto;
        }
    }
}
