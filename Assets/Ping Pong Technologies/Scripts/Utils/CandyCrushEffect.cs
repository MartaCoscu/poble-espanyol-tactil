﻿using UnityEngine;
using System.Collections;

public class CandyCrushEffect : MonoBehaviour
{

    public Vector3 minSize = Vector3.one;
    public Vector3 maxSize = Vector3.one * 2;

    private Vector3 actualSize;
    private Vector3 increasedSizeY = Vector3.zero;
    private Vector3 increasedSizeX = Vector3.zero;

    private  float direccioY = 1;
    private float direccioX = 1;
    public float increaseSizeY = 0.01f;
    public float increaseSizeX = 0.01f;

    public void Start()
    {
        transform.localScale = new Vector3(maxSize.x, minSize.y, transform.localScale.z);
        actualSize = transform.localScale;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
                if (transform.localScale.y > maxSize.y || transform.localScale.y < minSize.y)
        {
            direccioY *= -1;
        }
        if (transform.localScale.x > maxSize.x || transform.localScale.x < minSize.x)
        {
            direccioX *= -1;
        }

        increasedSizeY += new Vector3(0, increaseSizeY * direccioY, 0);
        increasedSizeY += new Vector3( increaseSizeX * direccioX,0, 0);


        transform.localScale = actualSize + increasedSizeY + increasedSizeX;
    }
}
