﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Text;
using UnityEngine.UI;





public class PPT
{
    public static void LoadDDSFromTXT(string ddsFile, GameObject tmp, System.Action<GameObject> result = null)
    {
        UnityThreading.ActionThread myThread = UnityThreadHelper.CreateThread(() =>
        {
            /*FileStream fs = new FileStream(pngFile, FileMode.Open, FileAccess.Read);

            Debug.Log("Loading png from << "+ pngFile);

            byte[] imageData2 = new byte[1024];
            fs.Read(imageData2, 0, 1024);
            //AGAFEM MIDES PNG.....
            float width = -1;
            float height = -1;
            // check only png tex!!! // http://www.libpng.org/pub/png/spec/1.2/PNG-Structure.html
            byte[] png_signature = { 137, 80, 78, 71, 13, 10, 26, 10 };

            const int cMinDownloadedBytes = 30;
            if (imageData2.Length > cMinDownloadedBytes)
            {
                // now we can check png format
                for (int i = 0; i < png_signature.Length; i++)
                {
                    if (imageData2[i] != png_signature[i])
                    {
                        Debug.LogWarning("Error! Texture os NOT png format!");
                        return; // this is NOT png file!
                    }
                }

                // now get width and height of texture
                width = imageData2[16] << 24 | imageData2[17] << 16 | imageData2[18] << 8 | imageData2[19];
                height = imageData2[20] << 24 | imageData2[21] << 16 | imageData2[22] << 8 | imageData2[23];
            }*/
            string[] info = System.IO.File.ReadAllText(ddsFile + ".txt").Split(':');

            // Debug.Log("width << " + info[0]);

            int width = int.Parse(info[0]);
            int height = int.Parse(info[1]);

            //Debug.Log("Size width << " + width + " height << " + height);

            //Debug.Log("Image or Giga foto file >> " + ddsFile);

            byte[] imageData = File.ReadAllBytes(ddsFile + ".dds");

            UnityThreadHelper.Dispatcher.Dispatch(
                () =>
                {
                    Texture2D tex = new Texture2D((int)width, (int)height, TextureFormat.BGRA32, false);
                    //txtr = new Texture2D(width, height, TextureFormat.DXT1, false);
                    tex.LoadRawTextureData(imageData);
                    tex.Apply();


                    if (tmp.GetComponent<Image>())
                    {
                        tex.wrapMode = TextureWrapMode.Clamp;
                        tmp.GetComponent<Image>().sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0, 0));//.zero);

                    }
                    else if (tmp.GetComponent<RawImage>())
                    {
                        tex.wrapMode = TextureWrapMode.Clamp;
                        tmp.GetComponent<RawImage>().texture = tex;//
                    }
                    else if (tmp.GetComponent<Renderer>())
                    {
                        tex.wrapMode = TextureWrapMode.Clamp;
                        tmp.GetComponent<Renderer>().material.mainTexture = tex;//
                    }

                    if (result != null)
                        result(tmp);

                    imageData = null;

                    tex = null;
                    //System.GC.Collect();
                }
            );
        });
    }

    internal static string GetPath(string p)
    {
        return Application.dataPath + "/../dataPPT/"+p;
    }

    public static void Serializer<T>(string path, T obj)
    {
        var serializer = new XmlSerializer(typeof(T));


        using (var stream = new FileStream(path, FileMode.Create))
        {
            var xmlWriter = new XmlTextWriter(stream, Encoding.UTF8);
            serializer.Serialize(xmlWriter, obj);
        }
    }


    public static T LoadSerializer<T>(string path)
    {
        var serializer = new XmlSerializer(typeof(T));
        using (var stream = new FileStream(path, FileMode.Open))
        {
            return (T)serializer.Deserialize(stream);
        }
    }


    internal static void SetNativeSize(RawImage rawImage)
    {
        rawImage.GetComponent<RectTransform>().sizeDelta = new Vector2(rawImage.GetComponent<RawImage>().texture.width, rawImage.GetComponent<RawImage>().texture.height);
    }

    //Loads the xml directly from the given string. Useful in combination with www.text.
    public static T LoadSerializerFromText<T>(string text)
    {
        var serializer = new XmlSerializer(typeof(T));
        return (T)serializer.Deserialize(new StringReader(text));
    }

    internal static IEnumerator LoadInto(string fileName, GameObject tmp, System.Action<GameObject> result = null, bool debug = false)
    {
        return LoadAsset<Texture2D>(fileName, (object t, string u) =>
        {
            if (tmp.GetComponent<Image>())
            {
                Texture2D tex = (Texture2D)t;
                tex.wrapMode = TextureWrapMode.Clamp;
                tmp.GetComponent<Image>().sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0, 0));//.zero);
				//tmp.GetComponent<Image>().SetNativeSize();

            }
            else if (tmp.GetComponent<RawImage>())
            {
                Texture2D tex = (Texture2D)t;
                tex.wrapMode = TextureWrapMode.Clamp;
                tmp.GetComponent<RawImage>().texture = tex;//
            }
            else if (tmp.GetComponent<Renderer>())
            {
                Texture2D tex = (Texture2D)t;
                tex.wrapMode = TextureWrapMode.Clamp;
                tmp.GetComponent<Renderer>().material.mainTexture = tex;//
            }

            if (result != null)
                result(tmp);
        }, debug);
    }

    internal static float Map(float x, float v1, float v2, float v3, float v4)
    {
        return v3 + (v4 - v3) * ((x - v1) / (v2 - v1));
    }

    public static void Identity(Transform fill, Transform pare = null)
    {
        if (pare != null)
            fill.SetParent(pare);
        fill.localPosition = Vector3.zero;
        fill.localScale = Vector3.one;
        fill.localRotation = Quaternion.identity;


    }

    public static void CopyPosition(Transform t_origne, Transform t_dst, Transform parent)
    {
        t_dst.SetParent(parent);
        t_dst.localPosition = t_origne.localPosition;
        t_dst.localScale = t_origne.localScale;
        t_dst.localRotation = t_origne.localRotation;


        if (t_dst.gameObject.GetComponent<RectTransform>() && t_origne.gameObject.GetComponent<RectTransform>())
        {
            t_dst.gameObject.GetComponent<RectTransform>().sizeDelta = t_origne.gameObject.GetComponent<RectTransform>().sizeDelta;
            t_dst.gameObject.GetComponent<RectTransform>().localPosition = t_origne.gameObject.GetComponent<RectTransform>().localPosition;
        }
    }



    internal static string TimeStamp()
    {
        //String timeStamp = DateTime.Now);
        return DateTime.Now.ToString("yyyyMMddHHmmssffff");
    }

    public static IEnumerator LoadAsset<T>(string url, System.Action<object, string> result = null, bool debug = false)
    {
        if (debug) Debug.Log("Loading asset >> " + url);

        if (typeof(T) == typeof(Texture2D))
        {
            if (debug) Debug.Log("Trying Resources... ");
            Texture2D texTmp = new Texture2D(128, 128);
            texTmp = Resources.Load<Texture2D>(url);
            if (texTmp != null)
            {
                result(texTmp, url);
                yield break;
            }
            if (debug) Debug.Log("Resources not found... ");
        }


        //first trying
        WWW www = new WWW(url);
        float elapsedTime = 0.0f;

        if (debug) Debug.Log("ElapsedTime >> " + elapsedTime);

        while (!www.isDone)
        {
            elapsedTime += Time.deltaTime;
            if (elapsedTime >= 10.0f) break;
            yield return null;
        }

        if (debug) Debug.Log("ElapsedTime >> " + elapsedTime);

        if (!www.isDone || !string.IsNullOrEmpty(www.error))
        {
            if (debug) Debug.LogError("Load Failed >> " + url);
            result(null, url);    // Pass null result.
            yield break;
        }
        else {

            if (debug) Debug.Log("Loaded >> " + url);

            if (typeof(T) == typeof(Texture2D))
            {
                Texture2D texTmp = new Texture2D(128, 128);
                www.LoadImageIntoTexture(texTmp);
                result(texTmp, url); // Pass retrieved result.
            }
            else {
                if (result != null)
                    result(null, url);    // Pass null result.
            }
        }
    }


    internal static long GetMillis()
    {
        return DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
    }
}
