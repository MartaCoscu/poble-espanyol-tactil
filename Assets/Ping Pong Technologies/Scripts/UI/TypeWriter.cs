﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Text))]
public class TypeWriter : MonoBehaviour
{
    private float letterPause = 0.000002f;

    public string message;
    private string[] words;

    public void SetMessage(string s)
    {
        message = s;
        //message = message.Replace("\n", " \n ");


        /*
        message = message.Replace("<b>", "");
        message = message.Replace("</b>", "");

        message = message.Replace("<sub>", "");
        message = message.Replace("</sub>", "");
        message = message.Replace("<i>", "");
        message = message.Replace("</i>", "");
        */

        float actualSize = gameObject.GetComponent<Text>().fontSize;

        actualSize -= 4;

        message = message.Replace("<sup>", "<size=" + actualSize + ">");
        message = message.Replace("</sup>", "</size>");

        message = message.Replace("<sub>", "<size=" + actualSize + ">");
        message = message.Replace("</sub>", "</size>");


        string[] tagsOpen = { "<b>", "<i>", "<size=" + actualSize + ">" };
        string[] tagsClose = { "</b>", "</i>", "</size>" };

        words = message.Split(' ');

        for (int e = 0; e < tagsOpen.Length; e++)
        {

            bool flag = false;
            bool startFlag = false;
            bool endFlag = false;

            //anem a mirar el tema dels <b>, etc...
            for (int i = 0; i < words.Length; i++)
            {
                //en cas de tenir bold...
                if (words[i].Contains(tagsOpen[e]))
                {
                    flag = true;
                    startFlag = true;
                }

                //en cas de finalitzzr bold...
                if (words[i].Contains(tagsClose[e]))
                {
                    endFlag = true;
                }

                //anilitzem tema flags
                if (flag)
                {
                    words[i] = (startFlag ? "" : tagsOpen[e]) + words[i] + (endFlag ? "" : tagsClose[e]);

                    if (startFlag)
                        startFlag = false;

                    if (endFlag)
                    {
                        flag = false;
                        endFlag = false;
                    }
                }
            }
        }


        gameObject.SetActive(false);
    }

    // Use this for initialization
    public void OnEnable()
    {
        //Debug.Log("start typewriter");
        gameObject.GetComponent<Text>().text = "";

        gameObject.GetComponent<Text>().text = "<b></b>"+ "<i></i>";

        StartCoroutine(TypeText());
    }

    IEnumerator TypeText()
    {
        foreach (string word in words)
        {
            gameObject.GetComponent<Text>().text += word + " ";
            //        yield return 0;
            yield return new WaitForSecondsRealtime(letterPause);
        }
    }
}