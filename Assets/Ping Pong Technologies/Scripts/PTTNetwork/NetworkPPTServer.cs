﻿/// <summary>
/// UNet LLAPI Hello World
/// This will establish a connection to a server socket from a Server socket, then send serialized data and deserialize it.
/// </summary>

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using System.Collections.Generic;

public class NetworkPPTClientInstance
{
    private GameObject tmp;

    public string reference
    {
        get
        {
            return tmp.name;
        }
    }

    public string idClient = "";

    public NetworkPPTClientInstance(GameObject tmp, int con)
    {
        this.tmp = tmp;
        time = PPT.GetMillis();

        this.connectionId = con;
    }

    public long time;
    internal int connectionId;

    internal GameObject GetGameObjectInstance()
    {
        return tmp;
    }

    internal void SetGameOnjectInstance(GameObject gameObject)
    {
        tmp = gameObject;
    }
}


public class NetworkPPTServer : NetworkPPT
{
    protected static NetworkPPTServer instance;

    public delegate void OnDataReceived(NetworkPPTMessage msg);
    public static OnDataReceived onDataReceived;



    public delegate void OnTransformReceived(string idClient, NetworkTransform trans);
    public static OnTransformReceived onTransformReceived;

    public GameObject prefabNetworkClientInstance;

    public List<NetworkPPTClientInstance> clients = new List<NetworkPPTClientInstance>();
    public  int timeOutClient = 2000;

    public void Start()
    {
        server = true;
        onConnectEvent += OnConnectedServerEvent;
        onDataEvent += OnDataServerEvent;
        onDisconnectEvent += OnDisconnectedServerEvent;
        Connect();
        instance = this;
        TestAliveConnections();
    }

    private void TestAliveConnections()
    {
        LeanTween.delayedCall(1, () =>
        {
            //Debug.Log("Test alive connections...");
            //Debug.Log("Connections << " + clients.Count);
            List<NetworkPPTClientInstance> deleteClients = new List<NetworkPPTClientInstance>();

            foreach (NetworkPPTClientInstance tmpI in clients)
            {
                if (tmpI.time + timeOutClient < PPT.GetMillis())
                {
                    deleteClients.Add(tmpI);
                }
            }

            foreach (NetworkPPTClientInstance i in deleteClients)
            {
                clients.Remove(i);
                Destroy(i.GetGameObjectInstance());
            }
            TestAliveConnections();
        });
    }

    private void OnDisconnectedServerEvent(NetworkPPTMessage m)
    {
        Debug.Log("Disconnect event Server.");

        //Transform t = transform.FindChild("c" + Connection + "ch" + channelId);
        NetworkPPTClientInstance removeInstance = null;

        foreach (NetworkPPTClientInstance tmpI in clients)
        {
            if (tmpI.reference.StartsWith("c" + m.connectionId))
            {
                removeInstance = tmpI;
                break;
            }
        }

        if (removeInstance != null)
        {
            Destroy(removeInstance.GetGameObjectInstance());
            clients.Remove(removeInstance);
        }
    }

    private void OnDataServerEvent(NetworkPPTMessage m)
    {
        //Debug.Log("Data event Server.");
        //Debug.Log("message type << " + m.GetMessageType() + " << " + m.idClient + " << " + m.connectionId + " << " + m.channelId);

        switch (m.type)
        {
            case NetworkPPTMessageType.ALIVE:
                bool trobat = false;
                foreach (NetworkPPTClientInstance tmpI in clients)
                {
                    if (tmpI.reference.StartsWith("c" + m.connectionId))
                    {
                        trobat = true;
                        tmpI.time = PPT.GetMillis();
                        tmpI.GetGameObjectInstance().name = "c" + m.connectionId + "--" + m.idClient;
                        tmpI.idClient = m.idClient;
                        break;
                    }
                }

                if (!trobat)
                {
                    CreateClientInstance(m.connectionId, m.idClient);
                }
                break;


            case NetworkPPTMessageType.DATA:
                if (onDataReceived != null)
                {
                    onDataReceived(m);
                }
                break;
            case NetworkPPTMessageType.TRANSFORM:
                if (onTransformReceived != null)
                {
                    onTransformReceived(m.idClient, JsonUtility.FromJson<NetworkTransform>(m.message));
                }
                break;
        }
    }

    private void OnConnectedServerEvent(NetworkPPTMessage m)
    {
        Debug.Log("Connected event Server.");
        //en  principi s'ha cerregat un client i mirem que passa...

        CreateClientInstance(m.connectionId);
    }

    private void CreateClientInstance(int connectionId, string idclient = null)
    {
        //mirem que no existeixi ja l'objecte...
        NetworkPPTClientInstance tmpInstance = null;

        foreach (NetworkPPTClientInstance tmpI in clients)
        {
            if (tmpI.reference.StartsWith("c" + connectionId))
            {
                tmpInstance = tmpI;
                break;
            }
        }

        if (tmpInstance == null)
        {
            GameObject tmp = Instantiate<GameObject>(prefabNetworkClientInstance);
            tmp.transform.SetParent(transform);

            tmp.name = ("c" + connectionId);
            tmpInstance = new NetworkPPTClientInstance(tmp, connectionId);
            clients.Add(tmpInstance);
        }
        else {
            bool founded = false;
            foreach (Transform t in transform)
            {
                if (t.name.StartsWith("c" + connectionId))
                {
                    tmpInstance.SetGameOnjectInstance(t.gameObject);
                    founded = true;
                    break;
                }
            }

            if (!founded)
            {
                GameObject tmp = Instantiate<GameObject>(prefabNetworkClientInstance);
                tmp.transform.SetParent(transform);

                tmp.name = "c" + connectionId;
                tmpInstance.SetGameOnjectInstance(tmp);
            }
        }

        if (idclient != null)
        {
            tmpInstance.idClient = idclient;
        }
    }

    internal static void SendData(string idClient, string v)
    {
        //en funcio del idclient... s'ha d'agafar la referencia...
        NetworkPPTClientInstance tmp = null;
        foreach (NetworkPPTClientInstance i in instance.clients)
        {
            if (i.idClient.CompareTo(idClient) == 0)
            {
                tmp = i;
                break;
            }
        }
        if (tmp != null)
        {
            NetworkPPTMessage m = new NetworkPPTMessage();
            m.type = NetworkPPTMessageType.DATA;
            m.idClient = idClient;
            m.message = v;
            //(idClient,tmp.connectionId,tmp.channelId, NetworkPPTMessageType.DATA, v);
            m.connectionId = tmp.connectionId;

            if (instance != null)
                instance.SendData(m);
        }
    }

    internal static bool ExistClient(string idClient)
    {
        if (instance != null)
        {
            if (instance.clients.Count > 0)
            {
                foreach (NetworkPPTClientInstance c in instance.clients)
                {
                    if (c.idClient.Equals(idClient))
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}