﻿/// <summary>
/// UNet LLAPI Hello World
/// This will establish a connection to a server socket from a client socket, then send serialized data and deserialize it.
/// </summary>

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;

[Serializable]
public class NetworkConfig
{
    public string ip = "";
    public string idClient = "";
    public int port = 7777;
}


public class NetworkPPTClient : NetworkPPT
{
    private static NetworkPPTClient instance;

    public delegate void OnDataReceived(NetworkPPTMessage m);
    public static OnDataReceived onDataReceived;
    private NetworkConfig configNetwork;

    public void Start()
    {
        /*
        PPT.Serializer<NetworkConfig>(Application.streamingAssetsPath + "/ConfigNetwork.xml", n);*/
        configNetwork = PPT.LoadSerializer<NetworkConfig>(Application.streamingAssetsPath + "/ConfigNetwork.xml");

        instance = this;

        port = configNetwork.port;
        ipServer = configNetwork.ip;
        id = configNetwork.idClient;

        server = false;
        onConnectEvent += OnConnectedClientEvent;
        onDataEvent += OnDataClientEvent;
        onDisconnectEvent += OnDisconnectedClientEvent;
        Connect();


        NetworkPPTMessage m = new NetworkPPTMessage();
        m.type = NetworkPPTMessageType.ALIVE;
        m.idClient = instance.id;
        m.message = "";

        SendData(m);

        //comencem a enviar alives...
        StartSendAliveData();

    }


    public static void SendTransform(Transform t)
    {
        if (instance != null)
        {
            NetworkPPTMessage m = new NetworkPPTMessage();
            m.idClient = instance.id;
            m.type = NetworkPPTMessageType.TRANSFORM;
            m.message = NetworkPPTTransform.GetJsonFromTransform(t);
            instance.SendData(m);
        }
    }

    public static void Send(string mesg)
    {
        if (instance != null)
        {
            NetworkPPTMessage m = new NetworkPPTMessage();
            m.idClient = instance.id;
            m.type = NetworkPPTMessageType.DATA;
            m.message = mesg;

            instance.SendData(m);
        }
    }

    private void StartSendAliveData()
    {
        if (instance != null)
        {
            LeanTween.delayedCall(1f, () =>
        {
            //Debug.Log("StartSendAliveData");
            if (client != null)
            {
                if (client.isConnected)
                {
                    // Debug.Log("StartSendAliveData event client.");
                    NetworkPPTMessage m = new NetworkPPTMessage();
                    m.idClient = instance.id;
                    m.type = NetworkPPTMessageType.ALIVE;
                    m.message = "";
                    SendData(m);
                }
            }
            StartSendAliveData();
        });
        }
    }

    private void OnDisconnectedClientEvent(NetworkPPTMessage m)
    {
        Debug.Log("Disconnect event client.");

        if (client != null)
        {
            client.Disconnect();
            client.Shutdown();
        }
        client = null;

        //Trying to reconnect...
        ReconnectToServer();
    }

    private void ReconnectToServer()
    {
        Debug.Log("ReconnectToServer event client.");
        if (instance != null)
        {
            Debug.Log("Instance != null");

            if (client != null)
            {
                Debug.Log("client != null");
                if (!client.isConnected)
                {
                    Debug.Log("client !isConnected");
                    client.Disconnect();
                    client.Shutdown();
                    Connect();
                    LeanTween.delayedCall(5, () =>
                    {
                        Debug.Log("LeanTween ReconnectToServer");
                        ReconnectToServer();
                    });
                }
            }
            else {
                Debug.Log("client == null");
                Connect();
                LeanTween.delayedCall(5, () =>
                {
                    ReconnectToServer();
                });
            }

        }
    }

    private void OnDataClientEvent(NetworkPPTMessage message)
    {
        Debug.Log("OnDataClientEvent event client. idClient << " + message.idClient);

        if (onDataReceived != null)
        {
            if (message.idClient.CompareTo(id) == 0)
            {
                onDataReceived(message);
            }
        }
    }

    private void OnConnectedClientEvent(NetworkPPTMessage m)
    {
        Debug.Log("OnConnectedClientEvent event client.");
    }
}