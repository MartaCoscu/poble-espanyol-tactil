﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Networking;



public class NetworkPPTMessageType
{
    public const short ALIVE = 160;
    public const short DATA = 161;
    public const short TRANSFORM = 162;
}

public class NetworkPPTMessage : MessageBase
{
    public string idClient;
    public string message;
    public short type;
    public int connectionId;
    internal bool proceed;
    internal int intent;
}

[Serializable]
public class NetworkPPTTransform
{
    [SerializeField]
    public Vector3 localPosition;

    [SerializeField]
    public Vector3 localScale;

    [SerializeField]
    public Vector3 localRotation;


    public static Transform GetTransformFromJson(String t)
    {
        NetworkPPTTransform tr = JsonUtility.FromJson<NetworkPPTTransform>(t);
        Transform n = new GameObject().transform;
        n.localPosition = n.localPosition;
        n.localScale = n.localScale;
        n.localRotation = n.localRotation;
        return n;

    }
    public NetworkPPTTransform(Transform transform)
    {
        this.localPosition = transform.localPosition;
        this.localScale = transform.localScale;
        this.localRotation = transform.localRotation.eulerAngles;
    }

    public NetworkPPTTransform(Vector3 pos, Vector3 scale, Vector3 rotation)
    {
        this.localPosition = pos;
        this.localScale = scale;
        this.localRotation = rotation;
    }

    public static string GetJsonFromTransform(Transform transform)
    {
        return new NetworkPPTTransform(transform).SaveToString();
    }

    public string SaveToString()
    {
        return JsonUtility.ToJson(this);

    }


}


public class NetworkPPT : MonoBehaviour
{
    public void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public static List<NetworkPPTMessage> pool = new List<NetworkPPTMessage>();
    public NetworkClient client;

    public string id;
    public int port = 7777;
    protected bool server = true;
    public string ipServer = "";

    public delegate void OnConnectEvent(NetworkPPTMessage netMsg);
    protected OnConnectEvent onConnectEvent;

    public delegate void OnDataEvent(NetworkPPTMessage message);
    protected OnDataEvent onDataEvent;

    public delegate void OnDisconnectEvent(NetworkPPTMessage netMsg);
    protected OnDisconnectEvent onDisconnectEvent;


    public void Connect()
    {
        if (server)
        {

            ConnectionConfig config = new ConnectionConfig();
            config.AddChannel(QosType.ReliableSequenced);
            config.AddChannel(QosType.Unreliable);
            NetworkServer.Configure(config, 100);


            NetworkServer.Listen(port);

            NetworkServer.RegisterHandler(MsgType.Connect, OnConnect);
            NetworkServer.RegisterHandler(MsgType.Disconnect, OnClientDisconnect);
            NetworkServer.RegisterHandler(MsgType.Error, OnClientError);

            NetworkServer.RegisterHandler(NetworkPPTMessageType.ALIVE, OnMessageIncomming);
            NetworkServer.RegisterHandler(NetworkPPTMessageType.TRANSFORM, OnMessageIncomming);
            NetworkServer.RegisterHandler(NetworkPPTMessageType.DATA, OnMessageIncomming);



            Debug.Log("NetworkBase Server Connect");
        }
        else {

            if (client != null)
            {
                if (client.isConnected)
                {
                    Debug.LogError("Already connected");
                    return;
                }
                else {
                    client.Disconnect();
                    client.Shutdown();
                    client = null;
                    //client.Connect(ipServer, port);
                    //client.ReconnectToNewHost(ipServer, port);
                    Debug.Log("client Connect");
                }
            }
            else {


                Debug.Log("new NetworkClient");
                client = new NetworkClient();

                ConnectionConfig config = new ConnectionConfig();
                config.AddChannel(QosType.ReliableSequenced);
                config.AddChannel(QosType.Unreliable);
                client.Configure(config, 100);

                client.Connect(ipServer, port);

                // system msgs
                client.RegisterHandler(MsgType.Connect, OnConnect);
                client.RegisterHandler(MsgType.Disconnect, OnClientDisconnect);
                client.RegisterHandler(MsgType.Error, OnClientError);

                client.RegisterHandler(NetworkPPTMessageType.ALIVE, OnMessageIncomming);
                client.RegisterHandler(NetworkPPTMessageType.TRANSFORM, OnMessageIncomming);
                client.RegisterHandler(NetworkPPTMessageType.DATA, OnMessageIncomming);

                client.Connect(ipServer, port);
            }


            Debug.Log("NetworkBase Client Connect");

        }
    }


    // --------------- System Handlers -----------------
    void OnMessageIncomming(NetworkMessage netMsg)
    {
        NetworkPPTMessage m = netMsg.ReadMessage<NetworkPPTMessage>();
        m.connectionId = netMsg.conn.connectionId;

        if (m.type != NetworkPPTMessageType.ALIVE)
            Debug.Log("Received << " + m.message + " type << " + m.type);
        if (onDataEvent != null)
        {
            if (m != null)
                onDataEvent(m);
        }
    }


    void OnConnect(NetworkMessage netMsg)
    {
        Debug.Log("OnConnect S'ha conectat client >> " + netMsg.conn.connectionId);
        if (onConnectEvent != null)
        {
            NetworkPPTMessage m = new NetworkPPTMessage();
            m.connectionId = netMsg.conn.connectionId;
            onConnectEvent(m);
        }
    }

    void OnClientDisconnect(NetworkMessage netMsg)
    {
        Debug.Log("OnClientDisconnect");
        if (onDisconnectEvent != null)
        {
            NetworkPPTMessage m = new NetworkPPTMessage();
            m.connectionId = netMsg.conn.connectionId;
            onDisconnectEvent(m);
        }
        ResetClient();
    }

    void OnClientError(NetworkMessage netMsg)
    {
        Debug.Log("OnClientError");
    }

    public void ResetClient()
    {
        if (client == null)
            return;

        client.Disconnect();
        client.Shutdown();
        client = null;

    }


    public void SendData(NetworkPPTMessage m)
    {

        if (server)
        {
            Debug.Log("Server add poll message");
            pool.Add(m);
        }
        else {
            if (client != null)
            {
                // Debug.Log("Client add poll message");
                pool.Add(m);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        byte[] buffer = new byte[1024];

        List<NetworkPPTMessage> poolDelet = new List<NetworkPPTMessage>();


        foreach (NetworkPPTMessage m in pool)
        {
            if (!server)
            {
                if (client != null)
                {
                    if (client.isConnected)
                    {
                        if (client.Send(m.type, m))
                        {
                            poolDelet.Add(m);
                        }
                        else {
                            if (m.type == NetworkPPTMessageType.ALIVE)
                            {
                                poolDelet.Add(m);
                            }
                        }
                    }
                    m.intent++;
                }
            }
            else {
                //en cas contrari... mirar a quin client s'ha d'enviar...
                NetworkServer.SendToClient(m.connectionId, m.type, m);
                m.proceed = true;
                poolDelet.Add(m);
            }
        }

        //eliminem missatges processats...
        foreach (NetworkPPTMessage m in poolDelet)
        {
            pool.Remove(m);
        }
    }

    void OnApplicationQuit()
    {
        if (server)
        {
            NetworkServer.Shutdown();
        }
        else {
            if (client != null)
            {
                client.Shutdown();
                client.Disconnect();
            }
        }
    }

}