﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System.Xml;
using System.Xml.Serialization;


public class Objecte 
{
    [XmlAttribute("ID")]
    public int Id=0;

    [XmlAttribute("MaxWidth")]
    public float MaxWidth=0f;
    [XmlAttribute("MinWidth")]
    public float MinWidth = 0f;

    [XmlAttribute("MaxHeight")]
    public float MaxHeight = 0f;
    [XmlAttribute("MinHeight")]
    public float MinHeight = 0f;

    [XmlArray("TreeOpitions"), XmlArrayItem("TreeOption")]
    public List<TreeOption> treeOptions = new List<TreeOption>();

    public Vector3 position;
    public int fingerId;


    public void ForceValues(float w, float h) {
        MinWidth = MaxWidth = w;
        MinHeight = MaxHeight = h;
    }

    public void SetValues(float w, float h) {
        if (this.MinHeight > h || this.MinHeight == 0)
        {
            this.MinHeight = h;
        }

        if (this.MinWidth > w || this.MinWidth == 0)
        {
            this.MinWidth = w;
        }

        if (this.MaxHeight <h || this.MaxHeight == 0)
        {
            this.MaxHeight = h;
        }

        if (this.MaxWidth < w || this.MaxWidth == 0)
        {
            this.MaxWidth = w;
        }
    }

}
