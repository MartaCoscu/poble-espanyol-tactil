﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DetectObjects : MonoBehaviour {
    public const int MAX_NUMBER = 10;

    public int simular_objecte = -1;

    public float delayedTimeToNewObject = 0.2f;
    public float delayedToRemoveCurrentObject = 3f;

    public int[] idObjecte;
    private int[] idtmpObjecte;

    private int[] idFinguer;

    private bool[] startDelay;
    private float[] tmpDelay;

    public Objecte[] obj;
    //Dictionary<int, Objecte> objectes =
	//    new Dictionary<int, Objecte>();

    public delegate void CreateNewObjecte(Objecte obj);
    public static CreateNewObjecte myCreateNewObject;

    public delegate void DestroyObject(Objecte obj);
    public static DestroyObject myDestroyObject;

    void Start() {
        //carreguem administrador d'objectes
        AdministradorObjectes.Load();

        idObjecte = new int[MAX_NUMBER];
        idtmpObjecte = new int[MAX_NUMBER];
        idFinguer = new int[MAX_NUMBER];
        startDelay = new bool[MAX_NUMBER];
        tmpDelay = new float[MAX_NUMBER];
        obj = new Objecte[MAX_NUMBER];

        //reiniciem objectes
        for (int i = 0; i < MAX_NUMBER; i++)
        {
            idObjecte[i] = -1;
            idFinguer[i] = 0;
            idtmpObjecte[i] = -1;
            obj[i] = null;
        }
    }

	// Update is called once per frame
    void Update()
    {

        //somi a agafar un ID
        Touch[] allTouches = TuioRectInput.touches;

        for (int e = 0; e < MAX_NUMBER;e++)
            idtmpObjecte[e] = -1;

        foreach (Touch t in allTouches)
        {
            Objecte tmpObj = AdministradorObjectes.container.GetObject(
                TuioRectInput.getWidth(t.fingerId) * Camera.main.pixelWidth,
                TuioRectInput.getHeight(t.fingerId) * Camera.main.pixelHeight);

            if (tmpObj != null)
            {
                obj[tmpObj.Id] = tmpObj;
                obj[tmpObj.Id].fingerId = t.fingerId;
                obj[tmpObj.Id].position = t.position;
                idtmpObjecte[tmpObj.Id] = obj[tmpObj.Id].Id;
            }
        }

        if (simular_objecte != -1) {
            //forcem un objecte
            Objecte tmpObj = new Objecte();
            tmpObj.fingerId = simular_objecte;
            tmpObj.Id = simular_objecte;

            obj[simular_objecte] = tmpObj;
            obj[simular_objecte].fingerId = 0;
            obj[simular_objecte].position = new Vector3(0, 0, 0);
            idtmpObjecte[simular_objecte] = obj[simular_objecte].Id;

        }

        for (int i = 0; i < MAX_NUMBER; i++)
        {
            //tenim un objecte.... mirem a veure si es el mateix id
            if (idObjecte[i] != idtmpObjecte[i])
            {
                //inicialitzem 
                if (!startDelay[i])
                {
                    startDelay[i] = true;
                    tmpDelay [i]= Time.time;
                }

                //Deixem un temps prudencial abans d'invocar nou element....
                if ((idtmpObjecte[i] == -1 ? (tmpDelay[i] + delayedToRemoveCurrentObject) : (tmpDelay[i] + delayedTimeToNewObject)) < Time.time)
                {
                    //fem que l'objecte sigui un de nou amb el concepte que id i tmp id sigui =
                    startDelay[i] = false;
                    idObjecte[i] = idtmpObjecte[i];
                    if (idObjecte[i] != -1)
                    {
                        myCreateNewObject(obj[i]);
                    }
                    else
                    {
                        myDestroyObject(obj[i]);
                        obj[i] = null;
                    }
                }
            }
            else
            {
                startDelay[i] = false;
            }
        }
    }

    internal bool GetObject(int id_objecte_tuio_detectar_per_canviar)
    {
        for (int i = 0; i < MAX_NUMBER; i++) {
            if(obj[i]!= null)
            {
                if (obj[i].Id == id_objecte_tuio_detectar_per_canviar)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
