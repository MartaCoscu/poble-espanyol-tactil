﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

[XmlRoot("ObjectCollection")]
public class ContenidorObjectes 
{
    [XmlArray("Objectes"), XmlArrayItem("Objecte")]
    public List<Objecte> Objectes = new List<Objecte>();


    public void Save(string path)
    {
        Debug.Log("Save file for the objectes : " + path);
        var serializer = new XmlSerializer(typeof(ContenidorObjectes));
        using (var stream = new FileStream(path, FileMode.Create))
        {
            var xmlWriter = new XmlTextWriter(stream, System.Text.Encoding.UTF8);
            serializer.Serialize(xmlWriter, this);
        }
    }

    public static ContenidorObjectes Load(string path)
    {
            Debug.Log("Loading file for the objectes : " + path);
        var serializer = new XmlSerializer(typeof(ContenidorObjectes));
        using (var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as ContenidorObjectes;
        }
    }

    public Objecte GetObject(int id)
    {
        foreach (Objecte o in Objectes)
        {
            if (o.Id == id)
                return o;
        }
        return null;
    }

    public Objecte GetObject(float w, float h)
    {
        foreach (Objecte o in Objectes)
        {
            if ((o.MinHeight < h && h < o.MaxHeight) && (o.MinWidth < w && w < o.MaxWidth))
            {
                return o;
            }
        }
        return null;
    }

    public void Save()
    {
        Save(Application.streamingAssetsPath + "/config.xml");
    }


    public void ReplaceObj(Objecte obj)
    {
        int id = 0;
        bool founded = false;
        foreach (Objecte o in Objectes)
        {
            if (o.Id == obj.Id)
            {
                founded = true;
                break;
            }
            id++;
        }
        if (founded)
            Objectes[id] = obj;
    }

    

    
}
