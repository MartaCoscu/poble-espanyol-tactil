﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ControlIDObjects : MonoBehaviour
{

    public GameObject calibracio;
    public GameObject aplicattion;

    public Text textResultat;
    public Image calibrationScreenImage;

    public Color inputWorking;
    public Color inputTesting;

    public Color inputTestingOk;

    private Color inputNotWorking;

    public bool recordInput;

    private bool objectesModificats = false;

    private Objecte obj;

    public int id = -1; //Identificador que fem servir per calibrar

    // Use this for initialization
    void Start()
    {
        AdministradorObjectes.Load();

        //TreeOption t=new TreeOption();

        //t.posicioInicial=new Vector3(2,2,0);


       // AdministradorObjectes.container.Objectes[0].treeOptions.Add(t);

       // AdministradorObjectes.Save();

        //Carreguem els objectes que hi ha en el xml
        inputNotWorking = calibrationScreenImage.color;
        recordInput = false;
    }

    // Update is called once per frame
    void Update()
    {
        textResultat.text = "";

        if (Input.GetKeyUp(KeyCode.C))
        {
            if (calibracio.activeSelf)
            {
                calibracio.SetActive(false);
                aplicattion.SetActive(true);
            }
            else
            {
                
                calibracio.SetActive(true);
                aplicattion.SetActive(false);

            }
        }

        if (calibracio.activeSelf)
        {
            for (int i = 0; i < 11; i++)
            if (Input.GetKeyUp(KeyCode.F1+i)) 
                id = i;

            if (Input.GetKeyUp(KeyCode.F12))
                id = -1;

            textResultat.text = "Waiting for object...";

            if (id != -1)
            {
                //l'aplicació de calibracio está oberta.... anem a calibrar els components
                Touch[] allTouches = TuioRectInput.touches;

                if (allTouches.Length == 1)
                {
                    Touch t = allTouches[0];

                    float w = (TuioRectInput.getWidth(t.fingerId) * Camera.main.pixelWidth);
                    float h = (TuioRectInput.getHeight(t.fingerId) * Camera.main.pixelHeight);

                    obj = AdministradorObjectes.container.GetObject(id);

                    if (recordInput)
                    {
                        if (obj != null)
                        {
                            calibrationScreenImage.color = inputWorking;
                            obj.SetValues(w, h);
                            objectesModificats = true;
                        }
                    }
                    else
                    {
                        if (objectesModificats)
                        {
                            objectesModificats = false;
                            if (obj != null)
                            {
                                Debug.Log("Saved!");
                                AdministradorObjectes.container.ReplaceObj(obj);
                                AdministradorObjectes.container.Save();
                                AdministradorObjectes.Load();
                            }
                        }
                        calibrationScreenImage.color = inputNotWorking;
                    }
                    textResultat.text = "Id Object : " + id + " -- Id : " + t.fingerId + " w : " + w + " h : " + h;
                }else{
                    calibrationScreenImage.color = inputNotWorking;
                    textResultat.text = "Id Object : " + id;
                }

                if (Input.GetKeyUp(KeyCode.R))
                {
                    obj = AdministradorObjectes.container.GetObject(id);
                    obj.ForceValues(0, 0);
                    AdministradorObjectes.container.ReplaceObj(obj);
                    AdministradorObjectes.container.Save();
                    AdministradorObjectes.Load();
                }

                if (Input.GetKeyUp(KeyCode.G))
                {
                    recordInput = !recordInput;
                }
            }
            else
            {
                //Anem a buscar si els ID x objectes funcionen
                calibrationScreenImage.color = inputTesting;
                Touch[] allTouches = TuioRectInput.touches;

                if (allTouches.Length == 1)
                {
                    Touch t = allTouches[0];

                    float w = (TuioRectInput.getWidth(t.fingerId) * Camera.main.pixelWidth);
                    float h = (TuioRectInput.getHeight(t.fingerId) * Camera.main.pixelHeight);

                    obj = AdministradorObjectes.container.GetObject(w, h);
                    if (obj != null)
                    {
                        calibrationScreenImage.color = inputTestingOk;
                        textResultat.text = "Id Object : " + obj.Id + " -- Id : " + t.fingerId
                            + "\n w : " + obj.MinWidth + "<" + w + "<" + obj.MaxWidth
                            + "\n h : " + obj.MinHeight + "<" + h + "<" + obj.MaxHeight;
                    }
                }
            }
        }
    }
}
