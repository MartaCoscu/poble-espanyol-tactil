﻿using UnityEngine;
using System.Collections;

public class AdministradorObjectes
{

    public static ContenidorObjectes container = null;

    public static void Load()
    {
        if (AdministradorObjectes.container == null)
            AdministradorObjectes.container = ContenidorObjectes.Load(Application.streamingAssetsPath + "/config.xml");
    }

    // Use this for initialization

    public static void Save()
    {
        AdministradorObjectes.container.Save(Application.streamingAssetsPath + "/config.xml");
    }

}
