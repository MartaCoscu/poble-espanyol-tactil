﻿using UnityEngine;
using System.Collections;
using RenderHeads.Media.AVProVideo;

public class SetVideo : MonoBehaviour {

	MediaPlayer mp;

	public string filepath;

	void OnEnable(){
		mp = gameObject.GetComponent<MediaPlayer> ();
	}

	public void LoadVideo(string file){
		filepath = file;
		Debug.Log ("Load Video " + PPT.GetPath (ControlGeneral.getLanguage () + "/" + filepath));
		mp.m_AutoStart = true;
		mp.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToProjectFolder, ControlGeneral.getLanguage () + "/" + filepath, true);
		mp.OpenVideoFromFile(MediaPlayer.FileLocation.AbsolutePathOrURL, PPT.GetPath (ControlGeneral.getLanguage () + "/" + filepath), true);

	
	}
}
