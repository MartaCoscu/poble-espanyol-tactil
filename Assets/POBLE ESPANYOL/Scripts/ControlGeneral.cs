﻿using UnityEngine;
using System.Collections;

//TODO: carregar aqui els contingus d'aquesta carpeta nomes i passar-lo als scripts
//TODO: idiomes
using System;


public class ControlGeneral : MonoBehaviour
{

//	private static int prevScene;

//	private static Settings settings;
	private static LlistaTextos llistaTextos;
	private static int language;
	private static PobleEspanyol poble; 

	private static string apartat; 
	private static string numFitxa = "Nord"; 

	private static PobleEspanyol[] pobles; 
	public void Awake()
	{
//		OnChangeScene();
	}

	/*
	public static Settings getInstanceSettings()
	{
		if (settings == null)
		{
			settings = PPT.LoadSerializer<Settings>(Application.streamingAssetsPath + "/settings.xml");
		}
		return settings;
	}
	*/
	/*
	public static App getInstanceApp()
	{
		if (app == null)
		{
			app = PPT.LoadSerializer<App>(Application.streamingAssetsPath + "/app.xml");
		}
		return app;
	}*/
	/*
	public static LlistaContinguts getLlistaContinguts()
	{
		if (llistaContinguts == null)
		{
			llistaContinguts = PPT.LoadSerializer<LlistaContinguts>(Application.streamingAssetsPath + "/contingut.xml");
		}
		return llistaContinguts;
	}
	*/
	/*
	public static LlistaContinguts getLlistaContingutsPrivats()
	{
		if (llistaContingutsPrivats == null)
		{
			llistaContingutsPrivats = PPT.LoadSerializer<LlistaContinguts>(Application.streamingAssetsPath + "/contingutPrivat.xml");
		}
		return llistaContingutsPrivats;
	}
	*/
	/*
	public static Contingut getContingutPublic()
	{
		return contingutPublic;
	}
	*/
	/*
	public static Contingut getContingutPrivat()
	{
		return contingutPrivat;
	}
	*/
/*	public static LlistaTextos getLlistaTextos()
	{
		if (llistaTextos == null)
		{
			llistaTextos = PPT.LoadSerializer<LlistaTextos>(PPT.GetPath("LANGUAGES/" + language + "/textos.xml"));
		}
		return llistaTextos;
	}
*/

	public static PobleEspanyol getPobleEspanyol(){
		poble = PPT.LoadSerializer<PobleEspanyol>(PPT.GetPath(language + "/poble.xml"));
		return poble;
	}

	public static PobleEspanyol GetThisPoble(int i){
		PobleEspanyol p;
		p = PPT.LoadSerializer<PobleEspanyol>(PPT.GetPath (i.ToString() + "/poble.xml"));	
		return p;
	}

	public static bool getRTL()
	{
		bool RTL = false;
		//if (llistaTextos == null)
		//{
		Idiomes idiomes = PPT.LoadSerializer<Idiomes>(PPT.GetPath("idiomes.xml"));
		foreach (Idioma i in idiomes.idiomes)
		{
			if (i.id.CompareTo(""+language) == 0)
			{
				RTL = i.RTL;
				break;
			}
		}
		//llistaTextos = PPT.LoadSerializer<LlistaTextos>(Application.streamingAssetsPath + "/LANGUAGES/" + language + "/textos.xml");
		//}
		return RTL;
	}
	/*
	public static LlistaMenus getInstaceMenu()
	{
		if (llistaMenus == null)
		{
			llistaMenus = PPT.LoadSerializer<LlistaMenus>(Application.streamingAssetsPath + "/menu.xml");
		}
		return llistaMenus;
	}
*/

	public static int getLanguage()
	{
		return language;
	}
/*
	public static int SetLastScene(int i)
	{
		lastScene = i;
		return lastScene;
	}
*/
	/*
	public static int GetLastScene()
	{
		return lastScene;
	}
	*/
	public static int setLanguage(int i)
	{

		language = i;
	//	llistaTextos = null;

		//try{
		Languages.ChangeLanguage();
		//}catch(Exception e){
		//	Debug.Log ("Per algun motiu ChangeLanguage llença excepcio...");
		//}
		return language;
	}

	public static string SetApartat(string newApartat){
		apartat = newApartat;

		return apartat;
	}

	public static string GetApartat(){
		return apartat;
	}

	public static string SetFitxa(string newFitxa){
		numFitxa = newFitxa;
		return numFitxa;
	}

	public static string GetFitxa(){
		return numFitxa;
	}
	/*
	public static string getCurrentFolder()
	{
		return currentFolder;
	}

	public static string setCurrentFolder(string s)
	{
		currentFolder = s;
		return currentFolder;
	}

	public static int getCurrentSceneModel()
	{
		return currentSceneModel;
	}

	public static int getLastSceneModel()
	{
		return lastScene;
	}

	public static int setCurrentSceneModel(int i)
	{
		lastScene = currentSceneModel;
		currentSceneModel = i;
		return currentSceneModel;
	}




	void OnChangeScene()
	{

		getLlistaContinguts();
		getLlistaContingutsPrivats();

		contingutPublic = new Contingut();
		for (int i = 0; i < llistaContinguts.llistaContinguts.Count; i++)
		{
			if (llistaContinguts.llistaContinguts[i].id == currentFolder)
			{
				contingutPublic = llistaContinguts.llistaContinguts[i];
			}
		}
		contingutPrivat = new Contingut();
		for (int i = 0; i < llistaContingutsPrivats.llistaContinguts.Count; i++)
		{
			if (llistaContingutsPrivats.llistaContinguts[i].id == currentFolder)
			{
				contingutPrivat = llistaContingutsPrivats.llistaContinguts[i];
			}
		}
	}*/
}
