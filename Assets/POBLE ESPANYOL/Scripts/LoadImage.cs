﻿using UnityEngine;
using System.Collections;

public class LoadImage : MonoBehaviour {

	public string url;

	void OnEnable () {
		if (url != "") {
			Load (url);
		}
	}


	public void Load(string myUrl){	
		url = myUrl;
	/*	string path = ControlGeneral.getLanguage () + "/" + url;

	//	Debug.Log ("file://" + PPT.GetPath (path));*/
		if (gameObject.activeInHierarchy) {

			StartCoroutine (PPT.LoadInto (("file://" + PPT.GetPath (ControlGeneral.getLanguage () + "/" + url)), gameObject, null, false));
		}
	}
}
