﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControlFitxaArquitectura : ControlFitxa {

	public FitxaArquitectura fitxa; 
	public Galeria galeria; 
	public CreateText titol; 
	public CreateText text; 
	public LoadImage mapaEspanya; 
	public LoadImage maskPoble; 
	public override void ChangeLangFitxa(){

		Debug.Log ("change lang fitxa");

		for (int i = 0; i < poble.Arquitectura.FitxesArquitectura.Count; i++) {
			
			if (poble.Arquitectura.FitxesArquitectura [i].id == ControlGeneral.GetFitxa ()) {
				Debug.Log ("fitxes arquitectura");
				fitxa = poble.Arquitectura.FitxesArquitectura [i];
			}
		}

		fitxaBase = fitxa;
		galeria.nomsTextures = fitxa.Galeria;
		galeria.Begin ();

		titol.theString = fitxa.titol; 
		titol.LoadText ();

		text.theString = fitxa.text;
		text.LoadText ();

		mapaEspanya.Load (fitxa.MapaEspanya); 

		maskPoble.Load (fitxa.mascaraMapa);

		titolGran.text = poble.Arquitectura.titol;
		ombraGran.text = poble.Arquitectura.titol;
	}


}
