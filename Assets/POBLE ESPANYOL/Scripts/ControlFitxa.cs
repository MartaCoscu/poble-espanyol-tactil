﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public abstract class ControlFitxa : Control {

	public string thumb; 
	public Fitxa fitxaBase; 
	public Text titolGran, ombraGran; 

	public override void ChangeLang(){
		Debug.Log ("change");
		ChangeLangFitxa ();
		thumb = fitxaBase.thumbnail; 
	}

	public abstract void ChangeLangFitxa();
}
