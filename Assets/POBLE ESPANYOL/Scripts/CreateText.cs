﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; 
using System.Collections.Generic;

public class CreateText : MonoBehaviour {

	public string theString; 
	Text text; 
	float speed; 
	public List<UILineInfo> l = new List<UILineInfo>();

	public List<string> linies = new List<string> ();

	public List<string> tempLinies = new List<string>(); 

	public List<string> saveLines = new List<string>();

	public string color; 

	void Awake(){
		text = gameObject.GetComponent<Text> ();
		speed = Random.Range (0.5f, 1.5f);
	//	speed = Random.Range (1f, 1f);
		text.color = new Color(0,0,0,0); 

	}


	public void LoadText () {

	//	theString = "Estoy escribiendo una <i>frase random</i> que <b>espero</b>  que luego  tenga sentido y sirva \npara animarla <i>correctamente</i> y eso";
	//	theString = "Estoy escribiendo una frase random que espero que luego tenga sentido y sirva para animarla correctamente y eso";

		text.horizontalOverflow = HorizontalWrapMode.Wrap;


		linies = new List<string> ();
		tempLinies = new List<string> (); 
		saveLines = new List<string> (); 


		text.text = theString; 

		StartCoroutine (DoLoad());
	}

	IEnumerator DoLoad(){
		yield return null; 
		yield return null;

		text.cachedTextGenerator.GetLines(l);

		for (int i = 0; i < l.Count; i++) {

			if (i < l.Count - 1) {
				linies.Add(theString.Substring (l [i].startCharIdx, l [i + 1].startCharIdx - l [i].startCharIdx));
				linies [i] = linies [i].Replace ("\n", ""); 
				tempLinies.Add(""); 
				saveLines.Add (""); 
			} else {
				linies.Add(theString.Substring (l [i].startCharIdx));
				tempLinies.Add(""); 
				saveLines.Add (""); 
			}

	//		text.text = linies [0].Substring(0, linies[0].Length-1) +"</i>";

			if (linies[i].Contains("<i>")){

				if (!linies [i].Substring (linies [i].IndexOf ("<i>")).Contains ("</i>")) {
					linies [i] = linies [i] + "</i>";
				}
			}

			if (linies[i].Contains("</i>")){

				if (!linies [i].Substring (0, linies [i].IndexOf ("</i>")).Contains ("<i>")) {
					linies [i] = "<i>" + linies[i];
				}
			}

			if (linies[i].Contains("<b>")){

				if (!linies [i].Substring (linies [i].IndexOf ("<b>")).Contains ("</b>")) {
					linies [i] = linies [i] + "</b>";
				}
			}

			if (linies[i].Contains("</b>")){

				if (!linies [i].Substring (0, linies [i].IndexOf ("</b>")).Contains ("<b>")) {
					linies [i] = "<b>" + linies[i];
				}
			}

		}

	//	tempLinies = linies;



		int larger = 0; 

		for (int i = 0; i < linies.Count; i++) {
			if (linies [i].Length > larger) {
				larger = linies[i].Length;
			}
		}


		LeanTween.value (gameObject, 0, larger, 1).setOnUpdate (KeepLoading).setOnComplete(FinalText); 
	}


	void FinalText(){

		for (int i = 0; i < linies.Count; i++) {
			saveLines[i] = "<color=" + color + "ff>" + linies[i] + "</color>";
		}

		text.text = ""; 
		for (int i = 0; i < saveLines.Count; i++) {
			string temp; 
			if (i == saveLines.Count-1) {
				temp = text.text + saveLines [i];
			} else {
				temp = text.text + saveLines [i] + "\n";			
			}
			text.text = temp;
			//	text.text = saveLines [0];
		}
	}

	void KeepLoading(float val){

		int index = (int)val; 
		//	tempLinies = linies;

		tempLinies = new List<string> (); 
		for (int i = 0; i < linies.Count; i++) {
			tempLinies.Add (linies [i]);
		}
			
		text.horizontalOverflow = HorizontalWrapMode.Overflow;
		text.text = ""; 

		for (int i = 0; i < linies.Count; i++) {
		
			if (linies [i].Length >= index + 0) {
				string temp = linies [i].Substring (0, index);


				if (temp.EndsWith ("<i>") || temp.EndsWith ("</i>") || temp.EndsWith ("<b>") || temp.EndsWith ("</b>")) {
				} else if (temp.EndsWith ("</i") || temp.EndsWith ("/i>") || temp.EndsWith ("</b") || temp.EndsWith ("/b>")) {
					temp = temp.Substring (0, temp.Length - 3);
				} else if (temp.EndsWith ("</") || temp.EndsWith ("<b") || temp.EndsWith ("b>")|| temp.EndsWith ("</") || temp.EndsWith ("/b") || temp.EndsWith ("</") || temp.EndsWith ("<b") || temp.EndsWith ("b>")|| temp.EndsWith ("/b")) {
					temp = temp.Substring (0, temp.Length - 2);
				} else if (temp.EndsWith ("<") || temp.EndsWith (">") || temp.EndsWith ("/")) {
					temp = temp.Substring (0, temp.Length - 1);
				}

				if (temp.StartsWith ("<i>") || temp.StartsWith ("</i>") || temp.StartsWith ("<b>") || temp.StartsWith ("</b>")) {
				} else if (temp.StartsWith ("</i") || temp.StartsWith ("/i>") || temp.StartsWith ("</b") || temp.StartsWith ("/b>")) {
					temp = temp.Substring (3);
				} else if (temp.StartsWith ("</") || temp.StartsWith ("<i") || temp.StartsWith ("i>")|| temp.StartsWith ("</") || temp.StartsWith ("/i") ||temp.StartsWith ("</") || temp.StartsWith ("<b") || temp.StartsWith ("b>")|| temp.StartsWith ("</") || temp.StartsWith ("/b")) {
					temp = temp.Substring (2);
				} else if (temp.StartsWith ("<") || temp.StartsWith (">") || temp.StartsWith ("/")) {
					temp = temp.Substring (1);
				}




				if (temp.Contains ("<i>")) {
					if (!temp.Substring (temp.IndexOf ("<i>")).Contains ("</i>")) {
						temp = temp + "</i>";
					}
				}

				if (temp.Contains ("</i>")) {

					if (!temp.Substring (0, temp.IndexOf ("</i>")).Contains ("<i>")) {
						temp = "<i>" + temp;
					}
				}

				if (temp.Contains ("<b>")) {
					if (!temp.Substring (temp.IndexOf ("<b>")).Contains ("</b>")) {
						temp = temp + "</b>";
					}
				}

				if (temp.Contains ("</b>")) {

					if (!temp.Substring (0, temp.IndexOf ("</b>")).Contains ("<b>")) {
						temp = "<b>" + temp;
					}
				}

				temp = "<color=" + color + "ff>" + temp + "</color>";

				tempLinies [i] = temp; 
				saveLines [i] = tempLinies [i];
			}
				
			if (linies [i].Length >= index + 1) {


				string temp = linies [i].Substring (index, 1);
				if (temp == "i" || temp == "b") {
					if (linies [i] [index + 1] == '>') {
						temp = ""; 
					}
				}



				if (temp.EndsWith ("<i>") || temp.EndsWith ("</i>") || temp.EndsWith ("<b>") || temp.EndsWith ("</b>")) {
				} else if (temp.EndsWith ("</i") || temp.EndsWith ("/i>") || temp.EndsWith ("</b") || temp.EndsWith ("/b>")) {
					temp = temp.Substring (0, temp.Length - 3);
				} else if (temp.EndsWith ("</") || temp.EndsWith ("<b") || temp.EndsWith ("b>")|| temp.EndsWith ("</") || temp.EndsWith ("/b") || temp.EndsWith ("</") || temp.EndsWith ("<b") || temp.EndsWith ("b>")|| temp.EndsWith ("/b")) {
					temp = temp.Substring (0, temp.Length - 2);
				} else if (temp.EndsWith ("<") || temp.EndsWith (">") || temp.EndsWith ("/")) {
					temp = temp.Substring (0, temp.Length - 1);
				}

				if (temp.StartsWith ("<i>") || temp.StartsWith ("</i>") || temp.StartsWith ("<b>") || temp.StartsWith ("</b>")) {
				} else if (temp.StartsWith ("</i") || temp.StartsWith ("/i>") || temp.StartsWith ("</b") || temp.StartsWith ("/b>")) {
					temp = temp.Substring (3);
				} else if (temp.StartsWith ("</") || temp.StartsWith ("<i") || temp.StartsWith ("i>")|| temp.StartsWith ("</") || temp.StartsWith ("/i") ||temp.StartsWith ("</") || temp.StartsWith ("<b") || temp.StartsWith ("b>")|| temp.StartsWith ("</") || temp.StartsWith ("/b")) {
					temp = temp.Substring (2);
				} else if (temp.StartsWith ("<") || temp.StartsWith (">") || temp.StartsWith ("/")) {
					temp = temp.Substring (1);
				}




				if (temp.Contains ("<i>")) {
					if (!temp.Substring (temp.IndexOf ("<i>")).Contains ("</i>")) {
						temp = temp + "</i>";
					}
				}

				if (temp.Contains ("</i>")) {

					if (!temp.Substring (0, temp.IndexOf ("</i>")).Contains ("<i>")) {
						temp = "<i>" + temp;
					}
				}

				if (temp.Contains ("<b>")) {
					if (!temp.Substring (temp.IndexOf ("<b>")).Contains ("</b>")) {
						temp = temp + "</b>";
					}
				}

				if (temp.Contains ("</b>")) {

					if (!temp.Substring (0, temp.IndexOf ("</b>")).Contains ("<b>")) {
						temp = "<b>" + temp;
					}
				}

				temp = "<color=" + color + "cc>" + temp + "</color>";
			

				tempLinies [i] = tempLinies [i] + temp;
				saveLines [i] = tempLinies [i];
			}

			if (linies [i].Length >= index + 2) {
				string temp = linies [i].Substring (index+1, 1);
				if (temp == "i" || temp == "b") {
					if (linies [i] [index+2] == '>') {
						temp = ""; 
					}
				}

				if (temp.EndsWith ("<i>") || temp.EndsWith ("</i>") || temp.EndsWith ("<b>") || temp.EndsWith ("</b>")) {
				} else if (temp.EndsWith ("</i") || temp.EndsWith ("/i>") || temp.EndsWith ("</b") || temp.EndsWith ("/b>")) {
					temp = temp.Substring (0, temp.Length - 3);
				} else if (temp.EndsWith ("</") || temp.EndsWith ("<b") || temp.EndsWith ("b>")|| temp.EndsWith ("</") || temp.EndsWith ("/b") || temp.EndsWith ("</") || temp.EndsWith ("<b") || temp.EndsWith ("b>")|| temp.EndsWith ("/b")) {
					temp = temp.Substring (0, temp.Length - 2);
				} else if (temp.EndsWith ("<") || temp.EndsWith (">") || temp.EndsWith ("/")) {
					temp = temp.Substring (0, temp.Length - 1);
				}

				if (temp.StartsWith ("<i>") || temp.StartsWith ("</i>") || temp.StartsWith ("<b>") || temp.StartsWith ("</b>")) {
				} else if (temp.StartsWith ("</i") || temp.StartsWith ("/i>") || temp.StartsWith ("</b") || temp.StartsWith ("/b>")) {
					temp = temp.Substring (3);
				} else if (temp.StartsWith ("</") || temp.StartsWith ("<i") || temp.StartsWith ("i>")|| temp.StartsWith ("</") || temp.StartsWith ("/i") ||temp.StartsWith ("</") || temp.StartsWith ("<b") || temp.StartsWith ("b>")|| temp.StartsWith ("</") || temp.StartsWith ("/b")) {
					temp = temp.Substring (2);
				} else if (temp.StartsWith ("<") || temp.StartsWith (">") || temp.StartsWith ("/")) {
					temp = temp.Substring (1);
				}




				if (temp.Contains ("<i>")) {
					if (!temp.Substring (temp.IndexOf ("<i>")).Contains ("</i>")) {
						temp = temp + "</i>";
					}
				}

				if (temp.Contains ("</i>")) {

					if (!temp.Substring (0, temp.IndexOf ("</i>")).Contains ("<i>")) {
						temp = "<i>" + temp;
					}
				}

				if (temp.Contains ("<b>")) {
					if (!temp.Substring (temp.IndexOf ("<b>")).Contains ("</b>")) {
						temp = temp + "</b>";
					}
				}

				if (temp.Contains ("</b>")) {

					if (!temp.Substring (0, temp.IndexOf ("</b>")).Contains ("<b>")) {
						temp = "<b>" + temp;
					}
				}


				temp = "<color=" + color + "99>" + temp + "</color>";


				tempLinies [i] = tempLinies [i] + temp;
				saveLines [i] = tempLinies [i];
			}

			if (linies [i].Length >= index + 3) {
				string temp = linies [i].Substring (index+2, 1);
				if (temp == "i" || temp == "b") {
					if (linies [i] [index+3] == '>') {
						temp = ""; 
					}
				}

				if (temp.EndsWith ("<i>") || temp.EndsWith ("</i>") || temp.EndsWith ("<b>") || temp.EndsWith ("</b>")) {
				} else if (temp.EndsWith ("</i") || temp.EndsWith ("/i>") || temp.EndsWith ("</b") || temp.EndsWith ("/b>")) {
					temp = temp.Substring (0, temp.Length - 3);
				} else if (temp.EndsWith ("</") || temp.EndsWith ("<b") || temp.EndsWith ("b>")|| temp.EndsWith ("</") || temp.EndsWith ("/b") || temp.EndsWith ("</") || temp.EndsWith ("<b") || temp.EndsWith ("b>")|| temp.EndsWith ("/b")) {
					temp = temp.Substring (0, temp.Length - 2);
				} else if (temp.EndsWith ("<") || temp.EndsWith (">") || temp.EndsWith ("/")) {
					temp = temp.Substring (0, temp.Length - 1);
				}

				if (temp.StartsWith ("<i>") || temp.StartsWith ("</i>") || temp.StartsWith ("<b>") || temp.StartsWith ("</b>")) {
				} else if (temp.StartsWith ("</i") || temp.StartsWith ("/i>") || temp.StartsWith ("</b") || temp.StartsWith ("/b>")) {
					temp = temp.Substring (3);
				} else if (temp.StartsWith ("</") || temp.StartsWith ("<i") || temp.StartsWith ("i>")|| temp.StartsWith ("</") || temp.StartsWith ("/i") ||temp.StartsWith ("</") || temp.StartsWith ("<b") || temp.StartsWith ("b>")|| temp.StartsWith ("</") || temp.StartsWith ("/b")) {
					temp = temp.Substring (2);
				} else if (temp.StartsWith ("<") || temp.StartsWith (">") || temp.StartsWith ("/")) {
					temp = temp.Substring (1);
				}




				if (temp.Contains ("<i>")) {
					if (!temp.Substring (temp.IndexOf ("<i>")).Contains ("</i>")) {
						temp = temp + "</i>";
					}
				}

				if (temp.Contains ("</i>")) {

					if (!temp.Substring (0, temp.IndexOf ("</i>")).Contains ("<i>")) {
						temp = "<i>" + temp;
					}
				}

				if (temp.Contains ("<b>")) {
					if (!temp.Substring (temp.IndexOf ("<b>")).Contains ("</b>")) {
						temp = temp + "</b>";
					}
				}

				if (temp.Contains ("</b>")) {

					if (!temp.Substring (0, temp.IndexOf ("</b>")).Contains ("<b>")) {
						temp = "<b>" + temp;
					}
				}

				temp = "<color=" + color + "77>" + temp + "</color>";


				tempLinies [i] = tempLinies [i] + temp;
				saveLines [i] = tempLinies [i];
			}
				

			if (linies [i].Length >= index + 4) {
				string temp = linies [i].Substring (index+3, 1);
				if (temp == "i" || temp == "b") {
					if (linies [i] [index+4]== '>') {
						temp = ""; 
					}
				}

				if (temp.EndsWith ("<i>") || temp.EndsWith ("</i>") || temp.EndsWith ("<b>") || temp.EndsWith ("</b>")) {
				} else if (temp.EndsWith ("</i") || temp.EndsWith ("/i>") || temp.EndsWith ("</b") || temp.EndsWith ("/b>")) {
					temp = temp.Substring (0, temp.Length - 3);
				} else if (temp.EndsWith ("</") || temp.EndsWith ("<b") || temp.EndsWith ("b>")|| temp.EndsWith ("</") || temp.EndsWith ("/b") || temp.EndsWith ("</") || temp.EndsWith ("<b") || temp.EndsWith ("b>")|| temp.EndsWith ("/b")) {
					temp = temp.Substring (0, temp.Length - 2);
				} else if (temp.EndsWith ("<") || temp.EndsWith (">") || temp.EndsWith ("/")) {
					temp = temp.Substring (0, temp.Length - 1);
				}

				if (temp.StartsWith ("<i>") || temp.StartsWith ("</i>") || temp.StartsWith ("<b>") || temp.StartsWith ("</b>")) {
				} else if (temp.StartsWith ("</i") || temp.StartsWith ("/i>") || temp.StartsWith ("</b") || temp.StartsWith ("/b>")) {
					temp = temp.Substring (3);
				} else if (temp.StartsWith ("</") || temp.StartsWith ("<i") || temp.StartsWith ("i>")|| temp.StartsWith ("</") || temp.StartsWith ("/i") ||temp.StartsWith ("</") || temp.StartsWith ("<b") || temp.StartsWith ("b>")|| temp.StartsWith ("</") || temp.StartsWith ("/b")) {
					temp = temp.Substring (2);
				} else if (temp.StartsWith ("<") || temp.StartsWith (">") || temp.StartsWith ("/")) {
					temp = temp.Substring (1);
				}




				if (temp.Contains ("<i>")) {
					if (!temp.Substring (temp.IndexOf ("<i>")).Contains ("</i>")) {
						temp = temp + "</i>";
					}
				}

				if (temp.Contains ("</i>")) {

					if (!temp.Substring (0, temp.IndexOf ("</i>")).Contains ("<i>")) {
						temp = "<i>" + temp;
					}
				}

				if (temp.Contains ("<b>")) {
					if (!temp.Substring (temp.IndexOf ("<b>")).Contains ("</b>")) {
						temp = temp + "</b>";
					}
				}

				if (temp.Contains ("</b>")) {

					if (!temp.Substring (0, temp.IndexOf ("</b>")).Contains ("<b>")) {
						temp = "<b>" + temp;
					}
				}
				temp = "<color=" + color + "44>" + temp + "</color>";


				tempLinies [i] = tempLinies [i] + temp;
				saveLines [i] = tempLinies [i];
			}

			if (linies [i].Length >= index + 5) {
				string temp = linies [i].Substring (index+4, 1);
				if (temp == "i" || temp == "b") {
					if (linies [i] [index+5]== '>') {
						temp = ""; 
					}
				}

				if (temp.EndsWith ("<i>") || temp.EndsWith ("</i>") || temp.EndsWith ("<b>") || temp.EndsWith ("</b>")) {
				} else if (temp.EndsWith ("</i") || temp.EndsWith ("/i>") || temp.EndsWith ("</b") || temp.EndsWith ("/b>")) {
					temp = temp.Substring (0, temp.Length - 3);
				} else if (temp.EndsWith ("</") || temp.EndsWith ("<b") || temp.EndsWith ("b>")|| temp.EndsWith ("</") || temp.EndsWith ("/b") || temp.EndsWith ("</") || temp.EndsWith ("<b") || temp.EndsWith ("b>")|| temp.EndsWith ("/b")) {
					temp = temp.Substring (0, temp.Length - 2);
				} else if (temp.EndsWith ("<") || temp.EndsWith (">") || temp.EndsWith ("/")) {
					temp = temp.Substring (0, temp.Length - 1);
				}

				if (temp.StartsWith ("<i>") || temp.StartsWith ("</i>") || temp.StartsWith ("<b>") || temp.StartsWith ("</b>")) {
				} else if (temp.StartsWith ("</i") || temp.StartsWith ("/i>") || temp.StartsWith ("</b") || temp.StartsWith ("/b>")) {
					temp = temp.Substring (3);
				} else if (temp.StartsWith ("</") || temp.StartsWith ("<i") || temp.StartsWith ("i>")|| temp.StartsWith ("</") || temp.StartsWith ("/i") ||temp.StartsWith ("</") || temp.StartsWith ("<b") || temp.StartsWith ("b>")|| temp.StartsWith ("</") || temp.StartsWith ("/b")) {
					temp = temp.Substring (2);
				} else if (temp.StartsWith ("<") || temp.StartsWith (">") || temp.StartsWith ("/")) {
					temp = temp.Substring (1);
				}




				if (temp.Contains ("<i>")) {
					if (!temp.Substring (temp.IndexOf ("<i>")).Contains ("</i>")) {
						temp = temp + "</i>";
					}
				}

				if (temp.Contains ("</i>")) {

					if (!temp.Substring (0, temp.IndexOf ("</i>")).Contains ("<i>")) {
						temp = "<i>" + temp;
					}
				}

				if (temp.Contains ("<b>")) {
					if (!temp.Substring (temp.IndexOf ("<b>")).Contains ("</b>")) {
						temp = temp + "</b>";
					}
				}

				if (temp.Contains ("</b>")) {

					if (!temp.Substring (0, temp.IndexOf ("</b>")).Contains ("<b>")) {
						temp = "<b>" + temp;
					}
				}
				temp = "<color=" + color + "22>" + temp + "</color>";


				tempLinies [i] = tempLinies [i] + temp;
				saveLines [i] = tempLinies [i];
			}

			if (linies [i].Length >= index + 6) {
				string temp = linies [i].Substring (index+5, 1);
				if (temp == "i") {
					if (linies [i] [index+6]== '>') {
						temp = ""; 
					}
				}

				if (temp.EndsWith ("<i>") || temp.EndsWith ("</i>") || temp.EndsWith ("<b>") || temp.EndsWith ("</b>")) {
				} else if (temp.EndsWith ("</i") || temp.EndsWith ("/i>") || temp.EndsWith ("</b") || temp.EndsWith ("/b>")) {
					temp = temp.Substring (0, temp.Length - 3);
				} else if (temp.EndsWith ("</") || temp.EndsWith ("<b") || temp.EndsWith ("b>")|| temp.EndsWith ("</") || temp.EndsWith ("/b") || temp.EndsWith ("</") || temp.EndsWith ("<b") || temp.EndsWith ("b>")|| temp.EndsWith ("/b")) {
					temp = temp.Substring (0, temp.Length - 2);
				} else if (temp.EndsWith ("<") || temp.EndsWith (">") || temp.EndsWith ("/")) {
					temp = temp.Substring (0, temp.Length - 1);
				}

				if (temp.StartsWith ("<i>") || temp.StartsWith ("</i>") || temp.StartsWith ("<b>") || temp.StartsWith ("</b>")) {
				} else if (temp.StartsWith ("</i") || temp.StartsWith ("/i>") || temp.StartsWith ("</b") || temp.StartsWith ("/b>")) {
					temp = temp.Substring (3);
				} else if (temp.StartsWith ("</") || temp.StartsWith ("<i") || temp.StartsWith ("i>")|| temp.StartsWith ("</") || temp.StartsWith ("/i") ||temp.StartsWith ("</") || temp.StartsWith ("<b") || temp.StartsWith ("b>")|| temp.StartsWith ("</") || temp.StartsWith ("/b")) {
					temp = temp.Substring (2);
				} else if (temp.StartsWith ("<") || temp.StartsWith (">") || temp.StartsWith ("/")) {
					temp = temp.Substring (1);
				}




				if (temp.Contains ("<i>")) {
					if (!temp.Substring (temp.IndexOf ("<i>")).Contains ("</i>")) {
						temp = temp + "</i>";
					}
				}

				if (temp.Contains ("</i>")) {

					if (!temp.Substring (0, temp.IndexOf ("</i>")).Contains ("<i>")) {
						temp = "<i>" + temp;
					}
				}

				if (temp.Contains ("<b>")) {
					if (!temp.Substring (temp.IndexOf ("<b>")).Contains ("</b>")) {
						temp = temp + "</b>";
					}
				}

				if (temp.Contains ("</b>")) {

					if (!temp.Substring (0, temp.IndexOf ("</b>")).Contains ("<b>")) {
						temp = "<b>" + temp;
					}
				}
				temp = "<color=" + color + "00>" + temp + "</color>";


				tempLinies [i] = tempLinies [i] + temp;
				saveLines [i] = tempLinies [i];
			}
		}

		for (int i = 0; i < saveLines.Count; i++) {
			string temp; 
			if (i == saveLines.Count-1) {
				temp = text.text + saveLines [i];
			} else {
				temp = text.text + saveLines [i] + "\n";			
			}
			text.text = temp;
		//	text.text = saveLines [0];
		}
	}
}
