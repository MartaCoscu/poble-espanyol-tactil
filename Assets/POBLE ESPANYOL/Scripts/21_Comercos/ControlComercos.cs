﻿using UnityEngine;
using System.Collections;
using RenderHeads.Media.AVProVideo;
using UnityEngine.UI;
using System.Collections.Generic; 
public class ControlComercos : ControlFitxa {

/*	public CreateText titol; 
	public CreateText text; */
	public SetVideoNew video; 		

	public Text temp, esquerra, dreta; 

	public GameObject mapa; 

	public List<UILineInfo> l = new List<UILineInfo>();
	//public MediaPlayer media; 

	public override void ChangeLangFitxa(){

		List<UILineInfo> l = new List<UILineInfo>();

		temp.text = poble.DescobreixMes.comercos.Text;

		video.gameObject.SetActive (false); 

		StartCoroutine (Proceed ()); 


		titolGran.text = poble.DescobreixMes.comercos.titol;
		ombraGran.text = poble.DescobreixMes.comercos.titol;



		mapa.GetComponent<PPTAnimation> ().onCompleteAnimationAction = () => {
			video.gameObject.SetActive (true); 
			video.LoadVideo (poble.DescobreixMes.comercos.VideoMapa);
		};
		mapa.GetComponent<PPTAnimation> ().LaunchAnimation (); 

	}

	IEnumerator Proceed(){
		yield return new WaitForSeconds(0.2f); 
		temp.cachedTextGenerator.GetLines(l);

		if (poble.DescobreixMes.comercos.Text != "") {
			dreta.gameObject.SetActive (true);
			esquerra.gameObject.SetActive (true);
			int tall = l.Count / 2; 
			esquerra.GetComponent<CreateText>().theString = temp.text.Substring (0, l [tall].startCharIdx - 1);
			esquerra.GetComponent<CreateText>().LoadText();
			dreta.GetComponent<CreateText>().theString = temp.text.Substring (l [tall].startCharIdx);
			dreta.GetComponent<CreateText>().LoadText(); 

		} else {
			dreta.gameObject.SetActive (false);
			esquerra.gameObject.SetActive (false);
		}


	}


	public void Update(){
		/* if (!play.activeSelf) {
			if (media.Control.IsFinished ()) {
				play.SetActive (true); 
			}
		}*/
	}
}
