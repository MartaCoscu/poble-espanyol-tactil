﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System;


public class Artesania:Base {
	[XmlArrayItem("SubmenuArtesania")]
	public new List<SubmenuArtesania> SubmenusArtesania;
}


public class SubmenuArtesania:Base{
	[XmlArrayItem("FitxaArtesania")]
	public List <FitxaArtesania> FitxesArtesania;
}


public class FitxaArtesania:Fitxa{
	[XmlArrayItem("Imatge")]
	public string[] Galeria;
	[XmlElement("PosicioPiu")]
	public Vector2 posicioPiu;
}