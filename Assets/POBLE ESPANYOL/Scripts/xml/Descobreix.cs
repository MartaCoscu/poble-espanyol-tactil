﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System;


public class Descobreix : Base {
	[XmlElement("Joc")]
	public Joc joc;
	[XmlElement("Videoguia")]
	public Videoguia videoguia; 
	[XmlElement("Gimcana")]
	public Gimcana gimcana; 
	[XmlElement("Gastronomia")]
	public Gastronomia gastronomia; 
	[XmlElement("Comercos")]
	public Comercos comercos; 
	[XmlElement("AltraBarcelona")]
	public AltraBarcelona altrabarcelona; 
}
