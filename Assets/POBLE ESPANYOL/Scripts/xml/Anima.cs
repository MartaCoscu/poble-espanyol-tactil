﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System;


public class Anima : Base {
	[XmlArrayItem("FitxaAnima")]
	public new List<FitxaAnima> FitxesAnima;
}


public class FitxaAnima : Fitxa{
	[XmlElement("Video")]
	public string video;
	[XmlElement("PosicioPiu")]
	public Vector2 posicioPiu;
}