﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System;

public class Fitxa : Base{
	[XmlElement("Text")]
	public string text;
}

public class Base {
	[XmlElement("Titol")]
	public string titol;
	[XmlElement("TitolBoto")]
	public string titolBoto;
	[XmlElement("Thumbnail")]
	public string thumbnail;
	[XmlElement("Fons")]
	public string fons;
	[XmlElement("Ordre")]
	public string ordre;
	public string id; 
}
	