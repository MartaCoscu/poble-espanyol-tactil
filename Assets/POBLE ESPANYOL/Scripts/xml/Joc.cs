﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System;


public class Joc : Base {
	public string TextPregunta;
	public string TextCorrectes; 
	public string TextIncorrectes; 
	public List<Nivell> Nivells = new List<Nivell> (); 
	public List<Pregunta> Preguntes = new List<Pregunta>(); 
}


public class Nivell{
//	public int EncertsMinims; 
	public string EncertsMaxims; 
	public string titol; 
	public string subtitol;
}

public class Pregunta{
	public string Enunciat; 
	[XmlArrayItem("Resposta")]
	public List<string> Respostes = new List<string>(); 
	public string Explicacio;

}

