﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System;


public class ArtContemporani : Base {
	[XmlArrayItem("FitxesArtContemporani")]
	public new List<FitxaAnima> fitxesAnima;
}


public class FitxaArtContemporani : Fitxa{
	[XmlElement("Video")]
	public string video;
	[XmlElement("PosicioPiu")]
	public Vector2 posicioPiu;
}