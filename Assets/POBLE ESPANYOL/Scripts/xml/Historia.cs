﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System;


public class Historia:Base {
	[XmlArrayItem("FitxaHistoria")]
	public new List<FitxaHistoria> FitxesHistoria;
}
	
public class FitxaHistoria:Fitxa{
	[XmlArrayItem("Imatge")]
	public string[] Galeria;

	public string Periode; 
	public string Subtitol; 
	public string TextPoble; 
	public string TextEspanya; 
	public string TextMon; 

	public string Poble; 
	public string Espanya; 
	public string Mon; 
}