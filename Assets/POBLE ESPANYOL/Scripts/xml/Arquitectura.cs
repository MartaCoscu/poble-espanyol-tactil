﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System;


public class Arquitectura:Base {
	[XmlArrayItem("FitxaArquitectura")]
	public new List<FitxaArquitectura> FitxesArquitectura;
}


public class FitxaArquitectura:Fitxa{
	[XmlArrayItem("Imatge")]
	public string[] Galeria;
	[XmlElement("MascaraMapa")]
	public string mascaraMapa;
	[XmlElement("MapaEspanya")]
	public string MapaEspanya;

}