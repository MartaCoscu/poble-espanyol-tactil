﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System;

[XmlRoot("llistaTextos")]
public class LlistaTextos{

	public bool RTL = false;
	[XmlArrayItem("textUnic")]
	public List<TextUnic> llista = new List<TextUnic>();
}

public class TextUnic {
	public string id;

	public string text;

	internal string GetText()
	{
		string tmp = text;
		tmp = tmp.Replace("<em>", "<i>");
		tmp = tmp.Replace("</em>", "</i>");
		return tmp;
	}

	/*public string Text
    {
        get
        {
            return this.text;
        }
        set
        {
            this.text = value.Replace("<em>", "<i>");
            this.text = this.text.Replace("</em>", "</i>");
            this.text = value;
        }
    }*/
}
