﻿using UnityEngine;
using System.Collections;

public class PobleEspanyol {
	public string titol; 
	public string language; 
	public string VideoStandBy;
	public string VideoIndividual;
	public string TextBotoEntrar;
	public Anima Anima; 
	public Arquitectura Arquitectura; 
	public Artesania Artesania;
	public ArtContemporani ArtContemporani;
	public Historia Historia;
	public Descobreix DescobreixMes;
}
