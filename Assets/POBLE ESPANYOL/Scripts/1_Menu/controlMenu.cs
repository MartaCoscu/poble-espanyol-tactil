﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class controlMenu : Control {

	public Text titol, titolOmbra;



	public Boto anima, arquitectura, artesania, art, historia, descobreix;



	override public void ChangeLang(){

		titol.text = poble.titol; 
		titolOmbra.text = poble.titol; 

		anima.SetImage (poble.Anima.thumbnail);
		arquitectura.SetImage (poble.Arquitectura.thumbnail);
		artesania.SetImage (poble.Artesania.thumbnail);
		art.SetImage (poble.ArtContemporani.thumbnail);
		historia.SetImage (poble.Historia.thumbnail);
		descobreix.SetImage (poble.DescobreixMes.thumbnail);

	}


}
