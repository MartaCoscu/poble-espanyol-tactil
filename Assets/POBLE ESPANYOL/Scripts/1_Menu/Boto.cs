﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Boto : MonoBehaviour {

	public int scene; 
	LoadImage img; 
	public string apartat; 
	public string numFitxa; 



	public Base myBase; 

	void OnEnable(){
		img = gameObject.GetComponent<LoadImage> ();
	}

	public void AutoSet(){
		SetImage (myBase.thumbnail);

		numFitxa = myBase.id;

	}

	// Update is called once per frame
	public void SetImage (string url) {
		
		img.Load (url);
	}

	public void Next(){
		if (apartat != "") {
			ControlGeneral.SetApartat (apartat);
		}

		if (numFitxa != ""){
			ControlGeneral.SetFitxa (numFitxa);
		}
		SceneManager.LoadScene (scene);
	}
}
