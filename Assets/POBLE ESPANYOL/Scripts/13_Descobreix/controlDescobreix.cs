﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class controlDescobreix : Control {

	public Boto Gastronomia, AltraBarcelona, Comercos, Joc, Gimcana, VideoGuia;
	public Text titol, titolOmbra;

	override public void ChangeLang(){


		titol.text = poble.DescobreixMes.titol; 
		titolOmbra.text = poble.DescobreixMes.titol; 


		Gastronomia.SetImage (poble.DescobreixMes.gastronomia.thumbnail);
		AltraBarcelona.SetImage (poble.DescobreixMes.altrabarcelona.thumbnail);
		Comercos.SetImage (poble.DescobreixMes.comercos.thumbnail);
		Joc.SetImage (poble.DescobreixMes.joc.thumbnail);
		Gimcana.SetImage (poble.DescobreixMes.gimcana.thumbnail);
		VideoGuia.SetImage (poble.DescobreixMes.videoguia.thumbnail);
	}


}
