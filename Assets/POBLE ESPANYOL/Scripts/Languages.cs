﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;

public class Languages : MonoBehaviour {

	int currentLang;

	public delegate void OnChangeLang();
	public static OnChangeLang onChangeLang;

	public static void ChangeLanguage(){
		try {
			if (onChangeLang != null)
				onChangeLang ();

		} catch (Exception e) {
			Debug.Log ("Error en ChangeLanguage... Exception.Message << " + e.Message );
		}
	}
}