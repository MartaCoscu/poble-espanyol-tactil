﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControlFitxaHistoria : ControlFitxa {

	public FitxaHistoria fitxa; 
	public Galeria galeria; 
	public CreateText titol; 
	public CreateText text, textPoble, textEspanya, textMon, periode; 
	public LoadImage iconapoble, iconaespana, iconamon; 

	public override void ChangeLangFitxa(){

		for (int i = 0; i < poble.Historia.FitxesHistoria.Count; i++) {
			
			if (poble.Historia.FitxesHistoria [i].id == ControlGeneral.GetFitxa ()) {
				fitxa = poble.Historia.FitxesHistoria [i];
			}
		}

		fitxaBase = fitxa;
		galeria.nomsTextures = fitxa.Galeria;
		galeria.Begin ();

		titol.theString = fitxa.titol; 
		titol.LoadText ();

		text.theString = fitxa.text;
		text.LoadText ();


		textPoble.theString = fitxa.TextPoble;
		textPoble.LoadText ();
		textEspanya.theString = fitxa.TextEspanya;
		textEspanya.LoadText (); 
		textMon.theString = fitxa.TextMon;
		textMon.LoadText (); 

		titolGran.text = poble.Historia.titol;
		ombraGran.text = poble.Historia.titol;

		periode.theString = fitxa.Periode; 
		periode.LoadText (); 

		iconamon.Load (fitxa.Mon); 
		iconapoble.Load (fitxa.Poble); 
		iconaespana.Load (fitxa.Espanya); 
	}


}
