﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; 
using System.Collections.Generic; 

public abstract class Icones : MonoBehaviour {

	public Sprite botoOn, botoOff; 

	public bool estat; //true = on; false = off

	public List<Icones> compas = new List<Icones>(); 

	// Use this for initialization
	void Start () {
		foreach (Transform child in transform.parent) {
			if (child != transform) {
				compas.Add (child.gameObject.GetComponent<Icones>()); 
			}
		}

		SetOff (); 
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Click(){
		if (estat == false) {
			SetOn (); 
		} else {
			SetOff (); 
		}
	}

	public void SetOn(){
		estat = true; 
		gameObject.GetComponent<Image> ().sprite = botoOn; 

		for (int i = 0; i < compas.Count; i++) {
			compas [i].SetOff ();
		}
		DoStuff ();
	}

	public void SetOff(){
		estat = false; 
		gameObject.GetComponent<Image> ().sprite = botoOff; 
		UndoStuff (); 
	}

	public abstract void DoStuff ();

	public abstract void UndoStuff ();

}
