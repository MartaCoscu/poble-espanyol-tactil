﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 
using UnityEngine.UI; 
using UnityEngine.SceneManagement; 

public class ControlJoc : Control {

	public CreateText textPregunta, textCorrectes, textIncorrectes;

	public CreateText pregunta, resposta0, resposta1, resposta2, explicacio; 

	public List<Pregunta> preguntes = new List<Pregunta> (); 

	public List<int> indexes = new List<int>(); 

	Joc joc = new Joc(); 

	public int correctes, incorrectes, current; 

	public List<RawImage> pastilles = new List<RawImage>(); 

	public Color correcte, incorrecte, seleccionat; 

	public List<float> ys = new List<float>(); 

	public GameObject totjoc, resultat; 


	public override void ChangeLang(){		

		joc = poble.DescobreixMes.joc; 

//		pregunta.theString = joc.TextPregunta;
//		correctes.theString = joc.TextCorrectes;
//		incorrectes.theString = joc.TextIncorrectes;

		for (int p = 0; p < pastilles.Count; p++) {
			ys.Add(pastilles[p].GetComponent<RectTransform>().localPosition.y);
		}

		int i = 0; 



		while (i < 5){
			int index = Random.Range(0, joc.Preguntes.Count); 

			if (indexes.Count == 0) {
				indexes.Add (index); 
				i++;
			} else {
				bool doadd = true;
				for (int n = 0; n < indexes.Count; n++) {
					if (index == indexes [n]) {
						doadd = false; 
					} 
				}
				if (doadd) {
					indexes.Add (index);
					i++;
				}
			}
		}
	
		for (int t = 0; t < indexes.Count; t++) {
			preguntes.Add (joc.Preguntes [indexes [t]]); 
		}

		SetQuestion (current); 
	}


	void SetQuestion(int i){
		current++; 
		explicacio.gameObject.GetComponent<Text> ().text = ""; 
		pastilles [0].color = Color.white; 
		pastilles [1].color = Color.white; 
		pastilles [2].color = Color.white; 



		List<int> mylist = new List<int> (); 

		while (mylist.Count < 3) {
			int r = Random.Range (0, 3);
			if (!mylist.Contains (r)) {
				mylist.Add (r);
			}
		}

		Debug.Log ("" + (mylist [0]) + (mylist [1]) + (mylist [2])); 
	
		pastilles[0].GetComponent<RectTransform>().localPosition = new Vector3(pastilles[0].transform.localPosition.x, ys [mylist [0]], pastilles[0].transform.localPosition.z); 
		pastilles[1].GetComponent<RectTransform>().localPosition = new Vector3(pastilles[1].transform.localPosition.x, ys [mylist [1]], pastilles[1].transform.localPosition.z); 
		pastilles[2].GetComponent<RectTransform>().localPosition = new Vector3(pastilles[2].transform.localPosition.x, ys [mylist [2]], pastilles[2].transform.localPosition.z); 

		pregunta.theString = preguntes [i].Enunciat; 
		pregunta.LoadText (); 

		resposta0.theString = preguntes [i].Respostes [0];
		resposta1.theString = preguntes [i].Respostes [1];
		resposta2.theString = preguntes [i].Respostes [2];

		resposta0.LoadText(); 
		resposta1.LoadText(); 
		resposta2.LoadText(); 

		explicacio.theString = preguntes [i].Explicacio; 
			
	}

	IEnumerator Reveal(int pressed){
		yield return new WaitForSeconds (1); 
		if (pressed == 0) {
			correctes++; 
		} else {
			incorrectes++;
		}

		explicacio.LoadText (); 
		pastilles [0].color = correcte; 
		pastilles [1].color = incorrecte; 
		pastilles [2].color = incorrecte; 


		yield return new WaitForSeconds (5); 
		if (current < 5) {
			SetQuestion (current); 
		} else {
			totjoc.SetActive (false); 
			resultat.SetActive (true); 
		}


	}

	public void Answer(int pressed){
		pastilles [pressed].color = seleccionat; 
		StartCoroutine (Reveal (pressed)); 

	}

	public void Restart(){
		SceneManager.LoadScene (22); 
	}
}
