﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ControlArtesania : Control {

	public List<Boto> botons = new List<Boto>(); 
	public GameObject prefab, mes; 
	public int maxim; 
	public int currentPage = 0; 
	public Text titolGran; 
	public Text ombraGran;
	public override void ChangeLang (){

		Debug.Log ("CHANGE LANG"); 


		for (int t = 0; t < poble.Artesania.SubmenusArtesania.Count; t++) {
				GameObject go = Instantiate (prefab); 
				go.transform.parent = prefab.transform.parent; 
				botons.Add (go.GetComponent<Boto> ()); 
		}

		mes.transform.SetAsLastSibling (); 
		mes.SetActive (true); 


		for (int i = 0; i < botons.Count; i++) {
			for (int t = 0; t < poble.Artesania.SubmenusArtesania.Count; t++) {
				//				Debug.Log (poble.Arquitectura.FitxesArquitectura[1].ordre);

				if (poble.Artesania.SubmenusArtesania [t].ordre == i.ToString ()) {
					botons [i].myBase = poble.Artesania.SubmenusArtesania [t];
					botons [i].AutoSet ();
				}
			}
		}

		titolGran.text = poble.Artesania.titol;
		ombraGran.text = poble.Artesania.titol;

		ShowTheButtons ();
		prefab.SetActive (false);  

	}



	public void NextPage(){

		if (botons.Count < maxim * (currentPage+1)) {
			currentPage = 0;
		} else {
			currentPage++; 
		}
		ShowTheButtons ();


	}



	public void ShowTheButtons(){
		if (botons.Count <= maxim) {
			for (int i = 0; i < botons.Count; i++) {
				botons [i].gameObject.SetActive (true); 
			}

			mes.SetActive (false);

		} else {


			if (botons.Count < maxim * (currentPage + 1)) {
				Debug.Log ("entra al primer if"); 
				for (int i = 0; i < botons.Count; i++) {
					if (i >= maxim * (currentPage) - 1 && i < (maxim * (currentPage + 1))) {
						botons [i].gameObject.SetActive (true); 
					} else {
						botons [i].gameObject.SetActive (false); 
					}
				}
			} else {
				Debug.Log ("entra al segon if"); 
				for (int i = 0; i < botons.Count; i++) {
					if (i >= maxim * (currentPage) && i < (maxim * (currentPage + 1)-1)) {
						botons [i].gameObject.SetActive (true); 
					} else {
						botons [i].gameObject.SetActive (false); 
					}
				}
			}

			mes.SetActive (true);
		}
	}
}
