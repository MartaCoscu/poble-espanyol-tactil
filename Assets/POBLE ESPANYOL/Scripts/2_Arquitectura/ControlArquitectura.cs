﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ControlArquitectura : Control {

	public List<Boto> botons = new List<Boto>(); 
	public Text titol, titolOmbra;


	public override void ChangeLang (){

		titol.text = poble.Arquitectura.titol; 
		titolOmbra.text = poble.Arquitectura.titol; 

		for (int i = 0; i < botons.Count; i++) {

			for (int t = 0; t < poble.Arquitectura.FitxesArquitectura.Count; t++) {
//				Debug.Log (poble.Arquitectura.FitxesArquitectura[1].ordre);

				if (poble.Arquitectura.FitxesArquitectura [t].ordre == i.ToString()) {
					botons [i].myBase = poble.Arquitectura.FitxesArquitectura [t];
					botons [i].AutoSet ();
				}
			}
		}
	}

}
