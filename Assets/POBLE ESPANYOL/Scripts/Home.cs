﻿using UnityEngine;
using System.Collections;

public class Home : Icones {

	public Back back;

	// Use this for initialization
	public override void DoStuff () {
		back.GoHome (); 
	}

	public override void UndoStuff(){
	}

}
