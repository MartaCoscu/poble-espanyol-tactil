﻿using UnityEngine;
using System.Collections;

public class SelectLanguage : MonoBehaviour {

	public Canvas canvas;
	private int idioma; 

	public void OnEnable(){
		Languages.ChangeLanguage();
	}


	// Update is called once per frame
	public void SetIdioma (int i) {
		idioma = i;
		canvas.GetComponent<PPTAnimation> ().inverse = false; 
		canvas.GetComponent<PPTAnimation> ().modules.onComplete.use = true;
		string[] a = new string[1];
		a [0] = "GoSetIdioma";
		canvas.GetComponent<PPTAnimation> ().modules.onComplete.messages = a;
		GameObject[] g = new GameObject[1];
		g [0] = gameObject;
		canvas.GetComponent<PPTAnimation> ().modules.onComplete.notifyGameObjects = g;
		canvas.GetComponent<PPTAnimation> ().LaunchAnimation ();
	}

	private void GoSetIdioma(){
		ControlGeneral.setLanguage (idioma);
		canvas.GetComponent<PPTAnimation> ().inverse = true; 
		canvas.GetComponent<PPTAnimation> ().modules.onComplete.use = false; 
		canvas.GetComponent<PPTAnimation> ().LaunchAnimation ();

	}
}
