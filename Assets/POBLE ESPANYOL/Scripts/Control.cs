﻿using UnityEngine;
using System.Collections;

abstract public class Control : MonoBehaviour {

	public PobleEspanyol poble;

	void Awake () {
		Languages.onChangeLang += ReloadPoble;
		Languages.onChangeLang += ChangeLang;
	}

	public void OnDisable()
	{
		Languages.onChangeLang -= ReloadPoble;
		Languages.onChangeLang -= ChangeLang;
	}

	public void OnDestroy()
	{
		Languages.onChangeLang -= ReloadPoble;
		Languages.onChangeLang -= ChangeLang;
	}

	public void ReloadPoble(){
		Debug.Log ("reload");
		poble = ControlGeneral.getPobleEspanyol ();
	}

	public abstract void ChangeLang(); 


}
