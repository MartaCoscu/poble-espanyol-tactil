﻿using UnityEngine;
using System.Collections;

public class Ubicacio : Icones {

	public GameObject mapa; 

	// Use this for initialization
	public override void DoStuff () {
		mapa.SetActive (true); 
		mapa.GetComponent<PPTAnimation> ().inverse = false;
		mapa.GetComponent<PPTAnimation> ().onCompleteAnimationAction = null; 
		mapa.GetComponent<PPTAnimation> ().LaunchAnimation ();
	}

	public override void UndoStuff(){
		mapa.GetComponent<PPTAnimation> ().inverse = true;
		mapa.GetComponent<PPTAnimation> ().onCompleteAnimationAction = () => {
			mapa.SetActive (false);
		};
		mapa.GetComponent<PPTAnimation> ().LaunchAnimation ();



	}
}
