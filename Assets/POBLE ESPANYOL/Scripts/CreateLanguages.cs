﻿using UnityEngine;
using System.Collections;
using System.IO; 
using System.Collections.Generic; 
using UnityEngine.UI;
public class CreateLanguages : MonoBehaviour {

	string[] langs; 

	public List<GameObject> botons; 

	GameObject prefab; 
	// Use this for initialization
	void Start () {
		prefab = transform.GetChild (0).gameObject; 
		langs = Directory.GetDirectories (Application.dataPath + "/../dataPPT"); 

		for (int i = 0; i < langs.Length; i++) {
			botons.Add(Instantiate (prefab)); 
			botons[i].transform.parent = transform; 
			botons [i].name = i.ToString(); 
			botons [i].transform.localPosition = new Vector2 (prefab.transform.localPosition.x, prefab.transform.localPosition.y - 27 * i); 
			botons [i].GetComponent<LoadLanguage> ().Load (ControlGeneral.GetThisPoble (i).language, i.ToString());
//			Debug.Log ("5"); 
		}
		Destroy (prefab);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
