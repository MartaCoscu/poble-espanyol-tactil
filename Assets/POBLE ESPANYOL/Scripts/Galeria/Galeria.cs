﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Galeria : MonoBehaviour {

	public List<GameObject> fotos = new List<GameObject>();

	int left = 0; 
	int center = 1; 
	int right = 2; 

	public bool block; 

	public string[] nomsTextures; 

	public int currentTexture = 0; 

	public GameObject negre; 

	public void Begin () {
		Unblock ();
	}

	public void GoRight(){

		if (!block) {


			negre.GetComponent<PPTAnimation> ().modules.alpha.init.assignCurrent = true;
			negre.GetComponent<PPTAnimation> ().modules.alpha.fi.floatValue = 0.3f;
			negre.GetComponent<PPTAnimation> ().LaunchAnimation (); 

			if (currentTexture - 1 != -1) {				
				currentTexture--;
			} else {
				currentTexture = nomsTextures.Length - 1;
			}
			block = true;

			int _right = center; 
			int _center = left; 
			int _left = right; 

			center = _center;
			right = _right; 
			left = _left;

			LeanTween.moveX (fotos [center], 0, 1).setOnComplete (Unblock); 
			LeanTween.moveX (fotos [right], 1080, 1).setOnComplete (Unblock); 
			fotos [left].transform.position = new Vector3 (-1080, fotos [left].transform.position.y, fotos [left].transform.position.z); 
		}
	}

	public void GoLeft(){

		if (!block) {

			negre.GetComponent<PPTAnimation> ().modules.alpha.init.assignCurrent = true;
			negre.GetComponent<PPTAnimation> ().modules.alpha.fi.floatValue = 0.3f;
			negre.GetComponent<PPTAnimation> ().LaunchAnimation (); 


			if (currentTexture + 1 != nomsTextures.Length) {
				currentTexture++;
			} else {
				currentTexture = 0; 
			}

			block = true; 
			int _right = left; 
			int _center = right; 
			int _left = center; 

			center = _center;
			right = _right; 
			left = _left;

			LeanTween.moveX (fotos [center], 0, 1).setOnComplete (Unblock); 
			LeanTween.moveX (fotos [left], -1080, 1).setOnComplete (Unblock); 
			fotos [right].transform.position = new Vector3 (1080, fotos [left].transform.position.y, fotos [left].transform.position.z); 

		}
	}


	public void Unblock(){

		negre.GetComponent<PPTAnimation> ().modules.alpha.init.assignCurrent = true;
		negre.GetComponent<PPTAnimation> ().modules.alpha.fi.floatValue = 0;
		negre.GetComponent<PPTAnimation> ().LaunchAnimation (); 


		if (currentTexture != nomsTextures.Length) {
			StartCoroutine (PPT.LoadInto (("file://" + PPT.GetPath (ControlGeneral.getLanguage () + "/" + nomsTextures[currentTexture])), fotos[center], null, false));
		} else {
			StartCoroutine (PPT.LoadInto (("file://" + PPT.GetPath (ControlGeneral.getLanguage () + "/" + nomsTextures[0])), fotos[center], null, false));
		}

		if (currentTexture + 1 != nomsTextures.Length) {
			StartCoroutine (PPT.LoadInto (("file://" + PPT.GetPath (ControlGeneral.getLanguage () + "/" + nomsTextures[currentTexture+1])), fotos[right], null, false));
		} else {
			StartCoroutine (PPT.LoadInto (("file://" + PPT.GetPath (ControlGeneral.getLanguage () + "/" + nomsTextures[0])), fotos[right], null, false));
		}

		if (currentTexture - 1 != -1) {
			Debug.Log ("current texture -1 " + (currentTexture - 1));
			StartCoroutine (PPT.LoadInto (("file://" + PPT.GetPath (ControlGeneral.getLanguage () + "/" + nomsTextures[currentTexture-1])), fotos[left], null, false));
		} else {
			StartCoroutine (PPT.LoadInto (("file://" + PPT.GetPath (ControlGeneral.getLanguage () + "/" + nomsTextures[nomsTextures.Length-1])), fotos[left], null, false));
		}

		block = false; 
	}

}
