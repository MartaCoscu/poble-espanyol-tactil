﻿using UnityEngine;
using System.Collections;
using RenderHeads.Media.AVProVideo;
using UnityEngine.UI;

public class ControlFitxaAnima : ControlFitxa {

	public FitxaAnima fitxa; 
	public CreateText titol; 
	public CreateText text; 
	public SetVideo video; 		
	public MediaPlayer media; 
	public GameObject play; 
	public GameObject myPiu;

	public override void ChangeLangFitxa(){

		for (int i = 0; i < poble.Anima.FitxesAnima.Count; i++) {

			if (poble.Anima.FitxesAnima[i].id == ControlGeneral.GetFitxa ()) {
				fitxa = poble.Anima.FitxesAnima[i];
			}
		}

		fitxaBase = fitxa;


		titol.theString = fitxa.titol; 
		titol.LoadText ();

		text.theString = fitxa.text;
		text.LoadText ();

		titolGran.text = poble.Anima.titol;
		ombraGran.text = poble.Anima.titol;

		video.LoadVideo (fitxa.video);

		myPiu.GetComponent<RectTransform>().anchoredPosition = new Vector2(fitxa.posicioPiu.x, fitxa.posicioPiu.y * -1);


	}



	public void Update(){
		if (!play.activeSelf) {
			if (media != null) {
				if (media.Control.IsFinished ()) {
					play.SetActive (true); 
				}
			}
		}
	}
}
