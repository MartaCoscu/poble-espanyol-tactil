﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ControlAnima : Control {

	public List<Boto> botons = new List<Boto>(); 

	public GameObject prefab; 

	public Text titol, titolOmbra;

	public override void ChangeLang (){

		titol.text = poble.Anima.titol; 
		titolOmbra.text = poble.Anima.titol; 

		for (int t = 0; t < poble.Anima.FitxesAnima.Count; t++) {
			Debug.Log ("anima"); 
			GameObject go = Instantiate (prefab); 
			go.transform.parent = prefab.transform.parent; 
			botons.Add(go.GetComponent<Boto>()); 
		}
		Destroy (prefab); 
		for (int i = 0; i < botons.Count; i++) {

			for (int t = 0; t < poble.Anima.FitxesAnima.Count; t++) {
				//				Debug.Log (poble.Arquitectura.FitxesArquitectura[1].ordre);

				if (poble.Anima.FitxesAnima [t].ordre == i.ToString()) {
					botons [i].myBase = poble.Anima.FitxesAnima [t];
					botons [i].AutoSet ();
				}
			}
		}
	}

}
